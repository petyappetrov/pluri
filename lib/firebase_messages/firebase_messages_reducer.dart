import 'package:pluri/firebase_messages/firebase_messages.dart';

FirebaseMessagesState firebaseMessagesReducer(
  FirebaseMessagesState state,
  action
) {
  if (action is FirebaseMessagesOnReceivedMessageAction) {
    return state.copyWith(
      message: action.message,
    );
  }
  if (action is FirebaseMessagesInitializeAction) {
    return state.copyWith(
      isInitializing: true
    );
  }
  if (action is FirebaseMessagesInitializeSuccessAction) {
    return state.copyWith(
      isInitializing: false,
    );
  }
  if (action is FirebaseMessagesInitializeFailureAction) {
    return state.copyWith(
      isInitializing: false,
      errorMessage: action.errorMessage,
    );
  }
  return state;
}
