// import 'package:pluri/notifications/notifications.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:pluri/firebase_messages/firebase_messages.dart';
// import 'package:redux_saga/redux_saga.dart';

// EventChannel firebaseMessagingConfigure () {
//   return EventChannel(subscribe: (emitter) {
//     FirebaseMessaging.onMessage.listen((RemoteMessage message) {
//       emitter(message)
//     })
//   })
//   // return EventChannel(subscribe: (emitter) {
//   //   firebaseMessaging.configure(
//   //     onMessage: (Map<String, dynamic> json) async {
//   //       try {
//   //         print(json);
//   //         FirebaseMessageEntity message = FirebaseMessageEntity.fromJson(json);
//   //         emitter(message);
//   //       } catch (e) {
//   //         print(e);
//   //       }
//   //     },
//   //     // onBackgroundMessage: myBackgroundMessageHandler,
//   //     onLaunch: (Map<String, dynamic> message) async {
//   //       print("onLaunch: $message");
//   //     },
//   //     onResume: (Map<String, dynamic> message) async {
//   //       print("onResume: $message");
//   //     },
//   //   );
//   //   return () {
//   //     firebaseMessaging.deleteInstanceID();
//   //   };
//   // });
// }

// initializeFirebaseMessages({ dynamic action }) sync* {
//   yield Try(() sync* {
//     final channel = Result<EventChannel>();

//     // yield Call(FirebaseMessaging, args: [false]);

//     yield Call(firebaseMessagingConfigure, result: channel);

//     firebaseMessaging.onIosSettingsRegistered.listen(
//       (IosNotificationSettings settings) {
//         print("Settings registered: $settings");
//       }
//     );

//     yield Put(
//       FirebaseMessagesInitializeSuccessAction()
//     );

//     while (true) {
//       final message = Result<FirebaseMessageEntity>();
//       yield Take(channel: channel.value, result: message);
//       yield Put(FirebaseMessagesOnReceivedMessageAction(message: message.value));
//       yield Put(NotificationsLoadAction());
//     }

//   }, Catch: (error) sync* {
//     yield Put(
//       FirebaseMessagesInitializeFailureAction(
//         errorMessage: 'Ошибка получения FCM токена',
//       )
//     );
//   });
// }

// requestFirebaseToken({ dynamic action }) sync* {
//   yield Try(() sync* {
//     yield Call(
//       firebaseMessaging.requestNotificationPermissions,
//       args: [
//         IosNotificationSettings(
//           sound: true,
//           badge: true,
//           alert: true,
//           provisional: false,
//         )
//       ]
//     );

//     final token = Result<String>();
//     yield Call(firebaseMessaging.getToken, result: token);
//     print(token.value);
//     if (token.value == null) {
//       throw 'Нет токена';
//     }

//     print('FCM Token: ${token.value}');

//     yield Put(
//       FirebaseMessagesRequestTokenSuccessAction(
//         firebaseToken: token.value
//       )
//     );
//   }, Catch: (error) sync* {
//     yield Put(
//       FirebaseMessagesRemoveTokenFailureAction(
//         errorMessage: 'Ошибка получения FCM токена',
//       )
//     );
//   });
// }

// removeFirebaseToken({ dynamic action }) sync* {
//   yield Try(() sync* {

//     yield Call(firebaseMessaging.deleteInstanceID);

//     yield Put(FirebaseMessagesRemoveTokenSuccessAction());

//   }, Catch: (error) sync* {
//     yield Put(
//       FirebaseMessagesRemoveTokenFailureAction(
//         errorMessage: 'Ошибка удаления FCM токена',
//       )
//     );
//   });
// }

// watchFirebaseMessagesInitialize({ dynamic action }) sync* {
//   yield TakeLatest(
//     initializeFirebaseMessages,
//     pattern: FirebaseMessagesInitializeAction
//   );
// }

// watchFirebaseMessagesRequestToken({ dynamic action }) sync* {
//   yield TakeLatest(
//     requestFirebaseToken,
//     pattern: FirebaseMessagesRequestTokenAction
//   );
// }

// watchFirebaseMessagesRemoveToken({ dynamic action }) sync* {
//   yield TakeLatest(
//     removeFirebaseToken,
//     pattern: FirebaseMessagesRemoveTokenAction
//   );
// }
