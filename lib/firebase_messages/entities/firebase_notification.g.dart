// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'firebase_notification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_FirebaseNotification _$_$_FirebaseNotificationFromJson(Map json) {
  return _$_FirebaseNotification(
    title: json['title'] as String,
    body: json['body'] as String,
  );
}

Map<String, dynamic> _$_$_FirebaseNotificationToJson(
        _$_FirebaseNotification instance) =>
    <String, dynamic>{
      'title': instance.title,
      'body': instance.body,
    };
