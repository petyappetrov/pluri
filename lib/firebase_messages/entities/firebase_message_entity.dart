import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:pluri/firebase_messages/firebase_messages.dart';

part 'firebase_message_entity.freezed.dart';
part 'firebase_message_entity.g.dart';

@freezed
abstract class FirebaseMessageEntity with _$FirebaseMessageEntity {
  @JsonSerializable(anyMap: true)
  factory FirebaseMessageEntity({
    required FirebaseNotification notification,
    required Map<String, dynamic> data,
  }) = _FirebaseMessageEntity;

  factory FirebaseMessageEntity.fromJson(Map<String, dynamic> json) =>
    _$FirebaseMessageEntityFromJson(json);
}
