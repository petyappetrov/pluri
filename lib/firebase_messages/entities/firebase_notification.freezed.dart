// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'firebase_notification.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

FirebaseNotification _$FirebaseNotificationFromJson(Map<String, dynamic> json) {
  return _FirebaseNotification.fromJson(json);
}

/// @nodoc
class _$FirebaseNotificationTearOff {
  const _$FirebaseNotificationTearOff();

  _FirebaseNotification call({required String title, required String body}) {
    return _FirebaseNotification(
      title: title,
      body: body,
    );
  }

  FirebaseNotification fromJson(Map<String, Object> json) {
    return FirebaseNotification.fromJson(json);
  }
}

/// @nodoc
const $FirebaseNotification = _$FirebaseNotificationTearOff();

/// @nodoc
mixin _$FirebaseNotification {
  String get title => throw _privateConstructorUsedError;
  String get body => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FirebaseNotificationCopyWith<FirebaseNotification> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FirebaseNotificationCopyWith<$Res> {
  factory $FirebaseNotificationCopyWith(FirebaseNotification value,
          $Res Function(FirebaseNotification) then) =
      _$FirebaseNotificationCopyWithImpl<$Res>;
  $Res call({String title, String body});
}

/// @nodoc
class _$FirebaseNotificationCopyWithImpl<$Res>
    implements $FirebaseNotificationCopyWith<$Res> {
  _$FirebaseNotificationCopyWithImpl(this._value, this._then);

  final FirebaseNotification _value;
  // ignore: unused_field
  final $Res Function(FirebaseNotification) _then;

  @override
  $Res call({
    Object? title = freezed,
    Object? body = freezed,
  }) {
    return _then(_value.copyWith(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      body: body == freezed
          ? _value.body
          : body // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$FirebaseNotificationCopyWith<$Res>
    implements $FirebaseNotificationCopyWith<$Res> {
  factory _$FirebaseNotificationCopyWith(_FirebaseNotification value,
          $Res Function(_FirebaseNotification) then) =
      __$FirebaseNotificationCopyWithImpl<$Res>;
  @override
  $Res call({String title, String body});
}

/// @nodoc
class __$FirebaseNotificationCopyWithImpl<$Res>
    extends _$FirebaseNotificationCopyWithImpl<$Res>
    implements _$FirebaseNotificationCopyWith<$Res> {
  __$FirebaseNotificationCopyWithImpl(
      _FirebaseNotification _value, $Res Function(_FirebaseNotification) _then)
      : super(_value, (v) => _then(v as _FirebaseNotification));

  @override
  _FirebaseNotification get _value => super._value as _FirebaseNotification;

  @override
  $Res call({
    Object? title = freezed,
    Object? body = freezed,
  }) {
    return _then(_FirebaseNotification(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      body: body == freezed
          ? _value.body
          : body // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(anyMap: true)
class _$_FirebaseNotification
    with DiagnosticableTreeMixin
    implements _FirebaseNotification {
  _$_FirebaseNotification({required this.title, required this.body});

  factory _$_FirebaseNotification.fromJson(Map<String, dynamic> json) =>
      _$_$_FirebaseNotificationFromJson(json);

  @override
  final String title;
  @override
  final String body;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FirebaseNotification(title: $title, body: $body)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'FirebaseNotification'))
      ..add(DiagnosticsProperty('title', title))
      ..add(DiagnosticsProperty('body', body));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _FirebaseNotification &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.body, body) ||
                const DeepCollectionEquality().equals(other.body, body)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(body);

  @JsonKey(ignore: true)
  @override
  _$FirebaseNotificationCopyWith<_FirebaseNotification> get copyWith =>
      __$FirebaseNotificationCopyWithImpl<_FirebaseNotification>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_FirebaseNotificationToJson(this);
  }
}

abstract class _FirebaseNotification implements FirebaseNotification {
  factory _FirebaseNotification({required String title, required String body}) =
      _$_FirebaseNotification;

  factory _FirebaseNotification.fromJson(Map<String, dynamic> json) =
      _$_FirebaseNotification.fromJson;

  @override
  String get title => throw _privateConstructorUsedError;
  @override
  String get body => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$FirebaseNotificationCopyWith<_FirebaseNotification> get copyWith =>
      throw _privateConstructorUsedError;
}
