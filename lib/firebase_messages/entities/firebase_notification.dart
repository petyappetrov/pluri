import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'firebase_notification.freezed.dart';
part 'firebase_notification.g.dart';

@freezed
abstract class FirebaseNotification with _$FirebaseNotification {
  @JsonSerializable(anyMap: true)
  factory FirebaseNotification({
    required String title,
    required String body,

  }) = _FirebaseNotification;

  factory FirebaseNotification.fromJson(Map<String, dynamic> json) =>
    _$FirebaseNotificationFromJson(json);
}
