import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:pluri/firebase_messages/firebase_messages.dart';

part 'firebase_messages_state.freezed.dart';
part 'firebase_messages_state.g.dart';

@freezed
abstract class FirebaseMessagesState with _$FirebaseMessagesState {
  factory FirebaseMessagesState({
    @Default(false) bool isInitializing,
    FirebaseMessageEntity? message,
    String? errorMessage,
    String? firebaseToken,
  }) = _FirebaseMessagesState;

  factory FirebaseMessagesState.fromJson(Map<String, dynamic> json) =>
    _$FirebaseMessagesStateFromJson(json);
}
