// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'firebase_messages_state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_FirebaseMessagesState _$_$_FirebaseMessagesStateFromJson(
    Map<String, dynamic> json) {
  return _$_FirebaseMessagesState(
    isInitializing: json['isInitializing'] as bool? ?? false,
    message: json['message'] == null
        ? null
        : FirebaseMessageEntity.fromJson(
            json['message'] as Map<String, dynamic>),
    errorMessage: json['errorMessage'] as String?,
    firebaseToken: json['firebaseToken'] as String?,
  );
}

Map<String, dynamic> _$_$_FirebaseMessagesStateToJson(
        _$_FirebaseMessagesState instance) =>
    <String, dynamic>{
      'isInitializing': instance.isInitializing,
      'message': instance.message,
      'errorMessage': instance.errorMessage,
      'firebaseToken': instance.firebaseToken,
    };
