// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'firebase_messages_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

FirebaseMessagesState _$FirebaseMessagesStateFromJson(
    Map<String, dynamic> json) {
  return _FirebaseMessagesState.fromJson(json);
}

/// @nodoc
class _$FirebaseMessagesStateTearOff {
  const _$FirebaseMessagesStateTearOff();

  _FirebaseMessagesState call(
      {bool isInitializing = false,
      FirebaseMessageEntity? message,
      String? errorMessage,
      String? firebaseToken}) {
    return _FirebaseMessagesState(
      isInitializing: isInitializing,
      message: message,
      errorMessage: errorMessage,
      firebaseToken: firebaseToken,
    );
  }

  FirebaseMessagesState fromJson(Map<String, Object> json) {
    return FirebaseMessagesState.fromJson(json);
  }
}

/// @nodoc
const $FirebaseMessagesState = _$FirebaseMessagesStateTearOff();

/// @nodoc
mixin _$FirebaseMessagesState {
  bool get isInitializing => throw _privateConstructorUsedError;
  FirebaseMessageEntity? get message => throw _privateConstructorUsedError;
  String? get errorMessage => throw _privateConstructorUsedError;
  String? get firebaseToken => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FirebaseMessagesStateCopyWith<FirebaseMessagesState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FirebaseMessagesStateCopyWith<$Res> {
  factory $FirebaseMessagesStateCopyWith(FirebaseMessagesState value,
          $Res Function(FirebaseMessagesState) then) =
      _$FirebaseMessagesStateCopyWithImpl<$Res>;
  $Res call(
      {bool isInitializing,
      FirebaseMessageEntity? message,
      String? errorMessage,
      String? firebaseToken});

  $FirebaseMessageEntityCopyWith<$Res>? get message;
}

/// @nodoc
class _$FirebaseMessagesStateCopyWithImpl<$Res>
    implements $FirebaseMessagesStateCopyWith<$Res> {
  _$FirebaseMessagesStateCopyWithImpl(this._value, this._then);

  final FirebaseMessagesState _value;
  // ignore: unused_field
  final $Res Function(FirebaseMessagesState) _then;

  @override
  $Res call({
    Object? isInitializing = freezed,
    Object? message = freezed,
    Object? errorMessage = freezed,
    Object? firebaseToken = freezed,
  }) {
    return _then(_value.copyWith(
      isInitializing: isInitializing == freezed
          ? _value.isInitializing
          : isInitializing // ignore: cast_nullable_to_non_nullable
              as bool,
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as FirebaseMessageEntity?,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String?,
      firebaseToken: firebaseToken == freezed
          ? _value.firebaseToken
          : firebaseToken // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }

  @override
  $FirebaseMessageEntityCopyWith<$Res>? get message {
    if (_value.message == null) {
      return null;
    }

    return $FirebaseMessageEntityCopyWith<$Res>(_value.message!, (value) {
      return _then(_value.copyWith(message: value));
    });
  }
}

/// @nodoc
abstract class _$FirebaseMessagesStateCopyWith<$Res>
    implements $FirebaseMessagesStateCopyWith<$Res> {
  factory _$FirebaseMessagesStateCopyWith(_FirebaseMessagesState value,
          $Res Function(_FirebaseMessagesState) then) =
      __$FirebaseMessagesStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool isInitializing,
      FirebaseMessageEntity? message,
      String? errorMessage,
      String? firebaseToken});

  @override
  $FirebaseMessageEntityCopyWith<$Res>? get message;
}

/// @nodoc
class __$FirebaseMessagesStateCopyWithImpl<$Res>
    extends _$FirebaseMessagesStateCopyWithImpl<$Res>
    implements _$FirebaseMessagesStateCopyWith<$Res> {
  __$FirebaseMessagesStateCopyWithImpl(_FirebaseMessagesState _value,
      $Res Function(_FirebaseMessagesState) _then)
      : super(_value, (v) => _then(v as _FirebaseMessagesState));

  @override
  _FirebaseMessagesState get _value => super._value as _FirebaseMessagesState;

  @override
  $Res call({
    Object? isInitializing = freezed,
    Object? message = freezed,
    Object? errorMessage = freezed,
    Object? firebaseToken = freezed,
  }) {
    return _then(_FirebaseMessagesState(
      isInitializing: isInitializing == freezed
          ? _value.isInitializing
          : isInitializing // ignore: cast_nullable_to_non_nullable
              as bool,
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as FirebaseMessageEntity?,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String?,
      firebaseToken: firebaseToken == freezed
          ? _value.firebaseToken
          : firebaseToken // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_FirebaseMessagesState implements _FirebaseMessagesState {
  _$_FirebaseMessagesState(
      {this.isInitializing = false,
      this.message,
      this.errorMessage,
      this.firebaseToken});

  factory _$_FirebaseMessagesState.fromJson(Map<String, dynamic> json) =>
      _$_$_FirebaseMessagesStateFromJson(json);

  @JsonKey(defaultValue: false)
  @override
  final bool isInitializing;
  @override
  final FirebaseMessageEntity? message;
  @override
  final String? errorMessage;
  @override
  final String? firebaseToken;

  @override
  String toString() {
    return 'FirebaseMessagesState(isInitializing: $isInitializing, message: $message, errorMessage: $errorMessage, firebaseToken: $firebaseToken)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _FirebaseMessagesState &&
            (identical(other.isInitializing, isInitializing) ||
                const DeepCollectionEquality()
                    .equals(other.isInitializing, isInitializing)) &&
            (identical(other.message, message) ||
                const DeepCollectionEquality()
                    .equals(other.message, message)) &&
            (identical(other.errorMessage, errorMessage) ||
                const DeepCollectionEquality()
                    .equals(other.errorMessage, errorMessage)) &&
            (identical(other.firebaseToken, firebaseToken) ||
                const DeepCollectionEquality()
                    .equals(other.firebaseToken, firebaseToken)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(isInitializing) ^
      const DeepCollectionEquality().hash(message) ^
      const DeepCollectionEquality().hash(errorMessage) ^
      const DeepCollectionEquality().hash(firebaseToken);

  @JsonKey(ignore: true)
  @override
  _$FirebaseMessagesStateCopyWith<_FirebaseMessagesState> get copyWith =>
      __$FirebaseMessagesStateCopyWithImpl<_FirebaseMessagesState>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_FirebaseMessagesStateToJson(this);
  }
}

abstract class _FirebaseMessagesState implements FirebaseMessagesState {
  factory _FirebaseMessagesState(
      {bool isInitializing,
      FirebaseMessageEntity? message,
      String? errorMessage,
      String? firebaseToken}) = _$_FirebaseMessagesState;

  factory _FirebaseMessagesState.fromJson(Map<String, dynamic> json) =
      _$_FirebaseMessagesState.fromJson;

  @override
  bool get isInitializing => throw _privateConstructorUsedError;
  @override
  FirebaseMessageEntity? get message => throw _privateConstructorUsedError;
  @override
  String? get errorMessage => throw _privateConstructorUsedError;
  @override
  String? get firebaseToken => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$FirebaseMessagesStateCopyWith<_FirebaseMessagesState> get copyWith =>
      throw _privateConstructorUsedError;
}
