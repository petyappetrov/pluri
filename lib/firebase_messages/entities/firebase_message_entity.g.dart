// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'firebase_message_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_FirebaseMessageEntity _$_$_FirebaseMessageEntityFromJson(Map json) {
  return _$_FirebaseMessageEntity(
    notification: FirebaseNotification.fromJson(
        Map<String, dynamic>.from(json['notification'] as Map)),
    data: Map<String, dynamic>.from(json['data'] as Map),
  );
}

Map<String, dynamic> _$_$_FirebaseMessageEntityToJson(
        _$_FirebaseMessageEntity instance) =>
    <String, dynamic>{
      'notification': instance.notification,
      'data': instance.data,
    };
