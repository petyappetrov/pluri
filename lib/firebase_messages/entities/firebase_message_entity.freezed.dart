// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'firebase_message_entity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

FirebaseMessageEntity _$FirebaseMessageEntityFromJson(
    Map<String, dynamic> json) {
  return _FirebaseMessageEntity.fromJson(json);
}

/// @nodoc
class _$FirebaseMessageEntityTearOff {
  const _$FirebaseMessageEntityTearOff();

  _FirebaseMessageEntity call(
      {required FirebaseNotification notification,
      required Map<String, dynamic> data}) {
    return _FirebaseMessageEntity(
      notification: notification,
      data: data,
    );
  }

  FirebaseMessageEntity fromJson(Map<String, Object> json) {
    return FirebaseMessageEntity.fromJson(json);
  }
}

/// @nodoc
const $FirebaseMessageEntity = _$FirebaseMessageEntityTearOff();

/// @nodoc
mixin _$FirebaseMessageEntity {
  FirebaseNotification get notification => throw _privateConstructorUsedError;
  Map<String, dynamic> get data => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FirebaseMessageEntityCopyWith<FirebaseMessageEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FirebaseMessageEntityCopyWith<$Res> {
  factory $FirebaseMessageEntityCopyWith(FirebaseMessageEntity value,
          $Res Function(FirebaseMessageEntity) then) =
      _$FirebaseMessageEntityCopyWithImpl<$Res>;
  $Res call({FirebaseNotification notification, Map<String, dynamic> data});

  $FirebaseNotificationCopyWith<$Res> get notification;
}

/// @nodoc
class _$FirebaseMessageEntityCopyWithImpl<$Res>
    implements $FirebaseMessageEntityCopyWith<$Res> {
  _$FirebaseMessageEntityCopyWithImpl(this._value, this._then);

  final FirebaseMessageEntity _value;
  // ignore: unused_field
  final $Res Function(FirebaseMessageEntity) _then;

  @override
  $Res call({
    Object? notification = freezed,
    Object? data = freezed,
  }) {
    return _then(_value.copyWith(
      notification: notification == freezed
          ? _value.notification
          : notification // ignore: cast_nullable_to_non_nullable
              as FirebaseNotification,
      data: data == freezed
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>,
    ));
  }

  @override
  $FirebaseNotificationCopyWith<$Res> get notification {
    return $FirebaseNotificationCopyWith<$Res>(_value.notification, (value) {
      return _then(_value.copyWith(notification: value));
    });
  }
}

/// @nodoc
abstract class _$FirebaseMessageEntityCopyWith<$Res>
    implements $FirebaseMessageEntityCopyWith<$Res> {
  factory _$FirebaseMessageEntityCopyWith(_FirebaseMessageEntity value,
          $Res Function(_FirebaseMessageEntity) then) =
      __$FirebaseMessageEntityCopyWithImpl<$Res>;
  @override
  $Res call({FirebaseNotification notification, Map<String, dynamic> data});

  @override
  $FirebaseNotificationCopyWith<$Res> get notification;
}

/// @nodoc
class __$FirebaseMessageEntityCopyWithImpl<$Res>
    extends _$FirebaseMessageEntityCopyWithImpl<$Res>
    implements _$FirebaseMessageEntityCopyWith<$Res> {
  __$FirebaseMessageEntityCopyWithImpl(_FirebaseMessageEntity _value,
      $Res Function(_FirebaseMessageEntity) _then)
      : super(_value, (v) => _then(v as _FirebaseMessageEntity));

  @override
  _FirebaseMessageEntity get _value => super._value as _FirebaseMessageEntity;

  @override
  $Res call({
    Object? notification = freezed,
    Object? data = freezed,
  }) {
    return _then(_FirebaseMessageEntity(
      notification: notification == freezed
          ? _value.notification
          : notification // ignore: cast_nullable_to_non_nullable
              as FirebaseNotification,
      data: data == freezed
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>,
    ));
  }
}

/// @nodoc

@JsonSerializable(anyMap: true)
class _$_FirebaseMessageEntity implements _FirebaseMessageEntity {
  _$_FirebaseMessageEntity({required this.notification, required this.data});

  factory _$_FirebaseMessageEntity.fromJson(Map<String, dynamic> json) =>
      _$_$_FirebaseMessageEntityFromJson(json);

  @override
  final FirebaseNotification notification;
  @override
  final Map<String, dynamic> data;

  @override
  String toString() {
    return 'FirebaseMessageEntity(notification: $notification, data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _FirebaseMessageEntity &&
            (identical(other.notification, notification) ||
                const DeepCollectionEquality()
                    .equals(other.notification, notification)) &&
            (identical(other.data, data) ||
                const DeepCollectionEquality().equals(other.data, data)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(notification) ^
      const DeepCollectionEquality().hash(data);

  @JsonKey(ignore: true)
  @override
  _$FirebaseMessageEntityCopyWith<_FirebaseMessageEntity> get copyWith =>
      __$FirebaseMessageEntityCopyWithImpl<_FirebaseMessageEntity>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_FirebaseMessageEntityToJson(this);
  }
}

abstract class _FirebaseMessageEntity implements FirebaseMessageEntity {
  factory _FirebaseMessageEntity(
      {required FirebaseNotification notification,
      required Map<String, dynamic> data}) = _$_FirebaseMessageEntity;

  factory _FirebaseMessageEntity.fromJson(Map<String, dynamic> json) =
      _$_FirebaseMessageEntity.fromJson;

  @override
  FirebaseNotification get notification => throw _privateConstructorUsedError;
  @override
  Map<String, dynamic> get data => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$FirebaseMessageEntityCopyWith<_FirebaseMessageEntity> get copyWith =>
      throw _privateConstructorUsedError;
}
