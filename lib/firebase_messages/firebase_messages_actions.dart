import 'package:pluri/firebase_messages/firebase_messages.dart';

class FirebaseMessagesOnReceivedMessageAction {
  final FirebaseMessageEntity message;
  FirebaseMessagesOnReceivedMessageAction({ required this.message });
}

class FirebaseMessagesOnReceivedBackgroundMessageAction {
  final FirebaseMessageEntity message;
  FirebaseMessagesOnReceivedBackgroundMessageAction({ required this.message });
}

class FirebaseMessagesInitializeAction {}

class FirebaseMessagesInitializeSuccessAction {
  FirebaseMessagesInitializeSuccessAction();
}

class FirebaseMessagesInitializeFailureAction {
  final String errorMessage;
  FirebaseMessagesInitializeFailureAction({ required this.errorMessage });
}

class FirebaseMessagesRequestTokenAction {}

class FirebaseMessagesRequestTokenSuccessAction {
  final String firebaseToken;
  FirebaseMessagesRequestTokenSuccessAction({ required this.firebaseToken });
}

class FirebaseMessagesRequestTokenFailureAction {
  final String errorMessage;
  FirebaseMessagesRequestTokenFailureAction({ required this.errorMessage });
}

class FirebaseMessagesRemoveTokenAction {}

class FirebaseMessagesRemoveTokenSuccessAction {}

class FirebaseMessagesRemoveTokenFailureAction {
  final String errorMessage;
  FirebaseMessagesRemoveTokenFailureAction({ required this.errorMessage });
}
