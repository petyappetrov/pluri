import 'package:dio/dio.dart';

final Dio dio = Dio(
  BaseOptions(
    connectTimeout: 5000,
    receiveTimeout: 5000,
    baseUrl: "http://192.168.1.13:8888/api/",
  )
);
