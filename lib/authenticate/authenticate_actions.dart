import 'package:image_picker/image_picker.dart';

class AuthenticateInitialzeAction {}

class AuthenticateInitialzeSuccessAction {
  final bool isAuthenticated;
  AuthenticateInitialzeSuccessAction({
    required this.isAuthenticated,
  });
}

class AuthenticateInitialzeFailureAction {
  final String errorMessage;
  AuthenticateInitialzeFailureAction({
    required this.errorMessage,
  });
}

class AuthenticateLogoutAction {}

class AuthenticateLogoutSuccessAction {}

class AuthenticateLogoutFailureAction {
  final String errorMessage;
  AuthenticateLogoutFailureAction({
    required this.errorMessage,
  });
}

class AuthenticateRegisterSubmitAction {
  final String name;
  final String email;
  final String password;
  final XFile? avatar;

  AuthenticateRegisterSubmitAction({
    required this.name,
    required this.email,
    required this.password,
    this.avatar,
  });
}

class AuthenticateRegisterSubmitSuccessAction {
  final String token;
  AuthenticateRegisterSubmitSuccessAction({
    required this.token,
  });
}

class AuthenticateRegisterSubmitFailureAction {
  final String errorMessage;
  AuthenticateRegisterSubmitFailureAction({
    required this.errorMessage,
  });
}


class AuthenticateLoginSubmitAction {
  final String email;
  final String password;

  AuthenticateLoginSubmitAction({
    required this.email,
    required this.password,
  });
}

class AuthenticateLoginSubmitSuccessAction {
  final String token;
  AuthenticateLoginSubmitSuccessAction({
    required this.token,
  });
}

class AuthenticateLoginSubmitFailureAction {
  final String errorMessage;
  AuthenticateLoginSubmitFailureAction({
    required this.errorMessage,
  });
}
