// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'authenticate_state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AuthenticateState _$_$_AuthenticateStateFromJson(Map<String, dynamic> json) {
  return _$_AuthenticateState(
    isInitializing: json['isInitializing'] as bool? ?? false,
    isAuthenticated: json['isAuthenticated'] as bool? ?? false,
    isSubmitting: json['isSubmitting'] as bool? ?? false,
    errorMessage: json['errorMessage'] as String?,
  );
}

Map<String, dynamic> _$_$_AuthenticateStateToJson(
        _$_AuthenticateState instance) =>
    <String, dynamic>{
      'isInitializing': instance.isInitializing,
      'isAuthenticated': instance.isAuthenticated,
      'isSubmitting': instance.isSubmitting,
      'errorMessage': instance.errorMessage,
    };
