import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'authenticate_state.freezed.dart';
part 'authenticate_state.g.dart';

@freezed
abstract class AuthenticateState with _$AuthenticateState {
  factory AuthenticateState({
    @Default(false) bool isInitializing,
    @Default(false) bool isAuthenticated,
    @Default(false) bool isSubmitting,
    String? errorMessage,
  }) = _AuthenticateState;

  factory AuthenticateState.fromJson(Map<String, dynamic> json) =>
    _$AuthenticateStateFromJson(json);
}
