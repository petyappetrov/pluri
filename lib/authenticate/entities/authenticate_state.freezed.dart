// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'authenticate_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AuthenticateState _$AuthenticateStateFromJson(Map<String, dynamic> json) {
  return _AuthenticateState.fromJson(json);
}

/// @nodoc
class _$AuthenticateStateTearOff {
  const _$AuthenticateStateTearOff();

  _AuthenticateState call(
      {bool isInitializing = false,
      bool isAuthenticated = false,
      bool isSubmitting = false,
      String? errorMessage}) {
    return _AuthenticateState(
      isInitializing: isInitializing,
      isAuthenticated: isAuthenticated,
      isSubmitting: isSubmitting,
      errorMessage: errorMessage,
    );
  }

  AuthenticateState fromJson(Map<String, Object> json) {
    return AuthenticateState.fromJson(json);
  }
}

/// @nodoc
const $AuthenticateState = _$AuthenticateStateTearOff();

/// @nodoc
mixin _$AuthenticateState {
  bool get isInitializing => throw _privateConstructorUsedError;
  bool get isAuthenticated => throw _privateConstructorUsedError;
  bool get isSubmitting => throw _privateConstructorUsedError;
  String? get errorMessage => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AuthenticateStateCopyWith<AuthenticateState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthenticateStateCopyWith<$Res> {
  factory $AuthenticateStateCopyWith(
          AuthenticateState value, $Res Function(AuthenticateState) then) =
      _$AuthenticateStateCopyWithImpl<$Res>;
  $Res call(
      {bool isInitializing,
      bool isAuthenticated,
      bool isSubmitting,
      String? errorMessage});
}

/// @nodoc
class _$AuthenticateStateCopyWithImpl<$Res>
    implements $AuthenticateStateCopyWith<$Res> {
  _$AuthenticateStateCopyWithImpl(this._value, this._then);

  final AuthenticateState _value;
  // ignore: unused_field
  final $Res Function(AuthenticateState) _then;

  @override
  $Res call({
    Object? isInitializing = freezed,
    Object? isAuthenticated = freezed,
    Object? isSubmitting = freezed,
    Object? errorMessage = freezed,
  }) {
    return _then(_value.copyWith(
      isInitializing: isInitializing == freezed
          ? _value.isInitializing
          : isInitializing // ignore: cast_nullable_to_non_nullable
              as bool,
      isAuthenticated: isAuthenticated == freezed
          ? _value.isAuthenticated
          : isAuthenticated // ignore: cast_nullable_to_non_nullable
              as bool,
      isSubmitting: isSubmitting == freezed
          ? _value.isSubmitting
          : isSubmitting // ignore: cast_nullable_to_non_nullable
              as bool,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$AuthenticateStateCopyWith<$Res>
    implements $AuthenticateStateCopyWith<$Res> {
  factory _$AuthenticateStateCopyWith(
          _AuthenticateState value, $Res Function(_AuthenticateState) then) =
      __$AuthenticateStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool isInitializing,
      bool isAuthenticated,
      bool isSubmitting,
      String? errorMessage});
}

/// @nodoc
class __$AuthenticateStateCopyWithImpl<$Res>
    extends _$AuthenticateStateCopyWithImpl<$Res>
    implements _$AuthenticateStateCopyWith<$Res> {
  __$AuthenticateStateCopyWithImpl(
      _AuthenticateState _value, $Res Function(_AuthenticateState) _then)
      : super(_value, (v) => _then(v as _AuthenticateState));

  @override
  _AuthenticateState get _value => super._value as _AuthenticateState;

  @override
  $Res call({
    Object? isInitializing = freezed,
    Object? isAuthenticated = freezed,
    Object? isSubmitting = freezed,
    Object? errorMessage = freezed,
  }) {
    return _then(_AuthenticateState(
      isInitializing: isInitializing == freezed
          ? _value.isInitializing
          : isInitializing // ignore: cast_nullable_to_non_nullable
              as bool,
      isAuthenticated: isAuthenticated == freezed
          ? _value.isAuthenticated
          : isAuthenticated // ignore: cast_nullable_to_non_nullable
              as bool,
      isSubmitting: isSubmitting == freezed
          ? _value.isSubmitting
          : isSubmitting // ignore: cast_nullable_to_non_nullable
              as bool,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AuthenticateState
    with DiagnosticableTreeMixin
    implements _AuthenticateState {
  _$_AuthenticateState(
      {this.isInitializing = false,
      this.isAuthenticated = false,
      this.isSubmitting = false,
      this.errorMessage});

  factory _$_AuthenticateState.fromJson(Map<String, dynamic> json) =>
      _$_$_AuthenticateStateFromJson(json);

  @JsonKey(defaultValue: false)
  @override
  final bool isInitializing;
  @JsonKey(defaultValue: false)
  @override
  final bool isAuthenticated;
  @JsonKey(defaultValue: false)
  @override
  final bool isSubmitting;
  @override
  final String? errorMessage;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'AuthenticateState(isInitializing: $isInitializing, isAuthenticated: $isAuthenticated, isSubmitting: $isSubmitting, errorMessage: $errorMessage)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'AuthenticateState'))
      ..add(DiagnosticsProperty('isInitializing', isInitializing))
      ..add(DiagnosticsProperty('isAuthenticated', isAuthenticated))
      ..add(DiagnosticsProperty('isSubmitting', isSubmitting))
      ..add(DiagnosticsProperty('errorMessage', errorMessage));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _AuthenticateState &&
            (identical(other.isInitializing, isInitializing) ||
                const DeepCollectionEquality()
                    .equals(other.isInitializing, isInitializing)) &&
            (identical(other.isAuthenticated, isAuthenticated) ||
                const DeepCollectionEquality()
                    .equals(other.isAuthenticated, isAuthenticated)) &&
            (identical(other.isSubmitting, isSubmitting) ||
                const DeepCollectionEquality()
                    .equals(other.isSubmitting, isSubmitting)) &&
            (identical(other.errorMessage, errorMessage) ||
                const DeepCollectionEquality()
                    .equals(other.errorMessage, errorMessage)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(isInitializing) ^
      const DeepCollectionEquality().hash(isAuthenticated) ^
      const DeepCollectionEquality().hash(isSubmitting) ^
      const DeepCollectionEquality().hash(errorMessage);

  @JsonKey(ignore: true)
  @override
  _$AuthenticateStateCopyWith<_AuthenticateState> get copyWith =>
      __$AuthenticateStateCopyWithImpl<_AuthenticateState>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_AuthenticateStateToJson(this);
  }
}

abstract class _AuthenticateState implements AuthenticateState {
  factory _AuthenticateState(
      {bool isInitializing,
      bool isAuthenticated,
      bool isSubmitting,
      String? errorMessage}) = _$_AuthenticateState;

  factory _AuthenticateState.fromJson(Map<String, dynamic> json) =
      _$_AuthenticateState.fromJson;

  @override
  bool get isInitializing => throw _privateConstructorUsedError;
  @override
  bool get isAuthenticated => throw _privateConstructorUsedError;
  @override
  bool get isSubmitting => throw _privateConstructorUsedError;
  @override
  String? get errorMessage => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$AuthenticateStateCopyWith<_AuthenticateState> get copyWith =>
      throw _privateConstructorUsedError;
}
