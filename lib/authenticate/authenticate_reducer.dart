import 'package:pluri/authenticate/authtenticate.dart';

AuthenticateState authenticateReducer(
  AuthenticateState state,
  action
) {
  if (action is AuthenticateInitialzeAction) {
    return state.copyWith(
      isInitializing: true,
    );
  }
  if (action is AuthenticateInitialzeSuccessAction) {
    return state.copyWith(
      isInitializing: false,
      isAuthenticated: action.isAuthenticated,
    );
  }
  if (action is AuthenticateInitialzeFailureAction) {
    return state.copyWith(
      isInitializing: false,
      errorMessage: action.errorMessage,
    );
  }
  if (action is AuthenticateLogoutAction) {
    return state.copyWith(
      isAuthenticated: false,
    );
  }

  if (action is AuthenticateRegisterSubmitAction) {
    return state.copyWith(
      isSubmitting: true,
      errorMessage: null,
    );
  }
  if (action is AuthenticateRegisterSubmitSuccessAction) {
    return state.copyWith(
      isSubmitting: false,
      errorMessage: null,
    );
  }
  if (action is AuthenticateRegisterSubmitFailureAction) {
    return state.copyWith(
      isSubmitting: false,
      errorMessage: action.errorMessage,
    );
  }
  return state;
}
