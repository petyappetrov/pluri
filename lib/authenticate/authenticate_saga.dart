import 'package:dio/dio.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:pluri/authenticate/authtenticate.dart';
import 'package:pluri/home/home.dart';
import 'package:pluri/profile/profile.dart';
import 'package:pluri/root/root.dart';
import 'package:redux_saga/redux_saga.dart';
import 'package:shared_preferences/shared_preferences.dart';

takeAuthTokenFromStorage() sync* {
  var prefs = Result<SharedPreferences>();

  yield Call(SharedPreferences.getInstance, result: prefs);

  final token = prefs.value?.getString('access_token');

  print('token: $token');

  yield Return(token);
}

removeAuthTokenFromStorage() sync* {
  var prefs = Result<SharedPreferences>();

  yield Call(SharedPreferences.getInstance, result: prefs);

  prefs.value?.remove('access_token');
}

saveAuthTokenToStorage({required action}) sync* {
  final prefs = Result<SharedPreferences>();

  yield Call(SharedPreferences.getInstance, result: prefs);

  prefs.value!.setString('access_token', action.token);

  yield Put(AuthenticateInitialzeAction());
}

initializeAuthenticate({dynamic action}) sync* {
  yield Try(() sync* {
    final token = Result<String>();

    yield Call(takeAuthTokenFromStorage, result: token);

    if (token.value == null) {
      yield Put(NavigateToAction.replace('/onboarding'));
    } else {
      yield Put(HomeChangeDestinationIndex(index: 0));
      yield Put(NavigateToAction.replace('/home'));
    }

    yield Put(AuthenticateInitialzeSuccessAction(
      isAuthenticated: token.value != null,
    ));
  }, Catch: (error) sync* {
    yield Put(AuthenticateInitialzeFailureAction(
      errorMessage: 'Ошибка авторизации',
    ));
  });
}

logout({dynamic action}) sync* {
  yield Try(() sync* {
    // yield Put(ProfileRemoveFirebaseTokenAction());

    // yield Take(pattern: ProfileRemoveFirebaseTokenSuccessAction);

    yield Call(removeAuthTokenFromStorage);

    yield Put(AuthenticateInitialzeAction());
  }, Catch: () sync* {
    yield Put(AuthenticateLogoutFailureAction(
      errorMessage: 'Не удалось выйти из аккаунта. Попробуйте еще раз.',
    ));
  });
}

register({required AuthenticateRegisterSubmitAction action}) sync* {
  yield Try(() sync* {
    // final timezone = Result<String>();
    // final firebaseToken = Result<String>();
    final token = Result<String>();
    final avatarUrl = Result<String>();

    yield Call(
      AuthenticateService.register,
      args: [
        action.name,
        action.email,
        action.password,
        // timezone.value,
        // firebaseToken.value,
      ],
      result: token,
    );

    yield Put(
      AuthenticateRegisterSubmitSuccessAction(
        token: token.value!,
      ),
    );

    yield Put(
      AuthenticateInitialzeAction(),
    );

    if (action.avatar != null) {
      yield Call(
        RootService.uploadPhoto,
        args: [action.avatar],
        result: avatarUrl,
      );
    }
  }, Catch: (error) sync* {
    print(error);
    if (error is DioError) {
      final message = error.response?.data['message'];

      yield Put(
        AuthenticateRegisterSubmitFailureAction(
          errorMessage: message ?? 'Серверная ошибка',
        ),
      );
    } else {
      yield Put(AuthenticateRegisterSubmitFailureAction(
        errorMessage: 'Серверная ошибка',
      ));
    }
  });
}

login({required AuthenticateLoginSubmitAction action}) sync* {
  yield Try(() sync* {
    final token = Result<String>();

    yield Call(
      AuthenticateService.login,
      args: [
        action.email,
        action.password,
      ],
      result: token,
    );

    yield Put(
      AuthenticateLoginSubmitSuccessAction(
        token: token.value!,
      ),
    );

    yield Put(
      AuthenticateInitialzeAction(),
    );
  }, Catch: (error) sync* {
    if (error is DioError) {
      final message = error.response?.data['message'];

      yield Put(
        AuthenticateRegisterSubmitFailureAction(
          errorMessage: message ?? 'Серверная ошибка',
        ),
      );
    } else {
      yield Put(AuthenticateLoginSubmitFailureAction(
        errorMessage: 'Серверная ошибка',
      ));
    }
  });
}

watchAuthenticateInitialize() sync* {
  yield TakeLatest(
    initializeAuthenticate,
    pattern: AuthenticateInitialzeAction,
  );
}

watchAuthenticateLogout() sync* {
  yield TakeLatest(logout, pattern: AuthenticateLogoutAction);
}

watchAuthenticateRegister() sync* {
  yield TakeLatest(register, pattern: AuthenticateRegisterSubmitAction);
}

watchAuthenticateLogin() sync* {
  yield TakeLatest(login, pattern: AuthenticateLoginSubmitAction);
}

watchAuthentcateSuccessRegister() sync* {
  yield TakeLatest(
    saveAuthTokenToStorage,
    pattern: [
      AuthenticateRegisterSubmitSuccessAction,
      AuthenticateLoginSubmitSuccessAction,
    ],
  );
}
