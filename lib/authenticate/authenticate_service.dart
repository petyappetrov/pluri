import 'package:pluri/http_client.dart';

class AuthenticateService {
  static Future<String> register(
    String name,
    String email,
    String password,
    // String? avatar,
    // String? timezone,
    // String? firebaseToken,
  ) async {
    final response = await dio.post(
      '/authentication/register',
      data: {
        'name': name,
        'email': email,
        'password': password,
        // 'avatar': avatar,
        // 'firebaseToken': firebaseToken
      },
    );
    return response.data['token'];
  }

  static Future<String> login(
    String email,
    String password,
  ) async {
    final response = await dio.post(
      '/authentication/login',
      data: {
        'email': email,
        'password': password,
      },
    );
    return response.data['token'];
  }
}
