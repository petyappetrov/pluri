import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart' as Hooks;
import 'package:flutter_redux/flutter_redux.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pluri/authenticate/authtenticate.dart';
import 'package:pluri/authenticate/screens/screens.dart';
import 'package:redux/redux.dart';
import 'package:pluri/root/root.dart';
import 'package:pluri/store/store.dart';

String emailPattern =
    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

class AuthenticateRegisterScreen extends Hooks.HookWidget {
  AuthenticateRegisterScreen({Key? key}) : super(key: key);

  final _formKey = GlobalKey<FormState>();

  final ImagePicker imagePicker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    final nameController = Hooks.useTextEditingController();

    final emailController = Hooks.useTextEditingController();

    final passwordController = Hooks.useTextEditingController();

    final selectedPhoto = Hooks.useState<XFile?>(null);

    final handleSelectPhoto = Hooks.useCallback((ImageSource source) async {
      final XFile? photo = await imagePicker.pickImage(source: source);

      if (photo != null) {
        selectedPhoto.value = photo;
      }
    }, []);

    final handleOpenSelectSource = Hooks.useCallback(() {
      if (Platform.isIOS) {
        showCupertinoModalPopup(
          context: context,
          builder: (context) => CupertinoActionSheet(
            actions: [
              CupertinoActionSheetAction(
                child: Text('Камера'),
                onPressed: () {
                  Navigator.pop(context);
                  handleSelectPhoto(ImageSource.camera);
                },
              ),
              CupertinoActionSheetAction(
                child: Text('Фотогалерея'),
                onPressed: () {
                  Navigator.pop(context);
                  handleSelectPhoto(ImageSource.gallery);
                },
              )
            ],
          ),
        );
      } else {
        showModalBottomSheet(
          context: context,
          builder: (context) => Wrap(children: [
            ListTile(
              leading: Icon(Icons.camera_alt),
              title: Text('Камера'),
              onTap: () {
                Navigator.pop(context);
                handleSelectPhoto(ImageSource.camera);
              },
            ),
            ListTile(
              leading: Icon(Icons.photo_album),
              title: Text('Фотогалерея'),
              onTap: () {
                Navigator.pop(context);
                handleSelectPhoto(ImageSource.gallery);
              },
            ),
          ]),
        );
      }
    }, []);

    return Scaffold(
      body: SingleChildScrollView(
        child: StoreConnector<AppState, _ViewModel>(
          converter: _ViewModel.fromStore,
          onWillChange: (prevState, nextState) {},
          builder: (context, vm) {
            return Container(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: SafeArea(
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 40),
                        child: Text(
                          'Регистрация',
                          style: TextStyle(
                            fontSize: 32,
                            height: 1.4,
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                      ),
                      SizedBox(height: 32),
                      GestureDetector(
                        onTap: () {
                          handleOpenSelectSource();
                        },
                        child: AvatarUI(
                          width: 90,
                          height: 90,
                          url: selectedPhoto.value?.path,
                        ),
                      ),
                      SizedBox(height: 32),
                      TextFormField(
                        controller: nameController,
                        decoration: InputDecoration(
                          labelText: 'Введите ваше имя',
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Пожалуйста заполните поле';
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: 24),
                      TextFormField(
                        controller: emailController,
                        decoration: InputDecoration(
                          labelText: 'Введите email',
                        ),
                        keyboardType: TextInputType.emailAddress,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Пожалуйста заполните поле';
                          }
                          if (!RegExp(emailPattern).hasMatch(value)) {
                            return 'Не верный формат почты';
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: 24),
                      TextFormField(
                        controller: passwordController,
                        keyboardType: TextInputType.visiblePassword,
                        obscureText: true,
                        decoration: InputDecoration(
                          labelText: 'Введите пароль',
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Пожалуйста заполните поле';
                          }
                          if (value.length < 6) {
                            return 'Длина пароля не должна быть не менее 6 символов';
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: 32),
                      SizedBox(
                        width: double.infinity,
                        child: ElevatedButton(
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              vm.onSubmit(
                                name: nameController.text,
                                email: emailController.text,
                                password: passwordController.text,
                                avatar: selectedPhoto.value,
                              );
                            }
                          },
                          child: Text(
                            'Отправить',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 16),
                      SizedBox(
                        width: double.infinity,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => AuthenticateLoginScreen(),
                              ),
                            );
                          },
                          child: Text(
                            'У меня уже есть аккаунт',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

class _ViewModel {
  final Function onSubmit;
  final bool isSubmitting;
  final String? errorMessage;

  _ViewModel({
    required this.onSubmit,
    required this.isSubmitting,
    required this.errorMessage,
  });

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
      onSubmit: ({
        required String name,
        required String email,
        required String password,
        XFile? avatar,
      }) {
        store.dispatch(AuthenticateRegisterSubmitAction(
          name: name,
          email: email,
          password: password,
          avatar: avatar,
        ));
      },
      isSubmitting: selectAuthenticateIsSubmitting(store.state),
      errorMessage: selectAuthenticateErrorMessage(store.state),
    );
  }
}
