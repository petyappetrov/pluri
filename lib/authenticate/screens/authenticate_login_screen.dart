import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart' as Hooks;
import 'package:flutter_redux/flutter_redux.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pluri/authenticate/authtenticate.dart';
import 'package:pluri/authenticate/screens/screens.dart';
import 'package:redux/redux.dart';
import 'package:pluri/store/store.dart';

class AuthenticateLoginScreen extends Hooks.HookWidget {
  AuthenticateLoginScreen({Key? key}) : super(key: key);

  final _formKey = GlobalKey<FormState>();

  final ImagePicker imagePicker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    final emailController = Hooks.useTextEditingController();

    final passwordController = Hooks.useTextEditingController();

    return Scaffold(
      body: SingleChildScrollView(
        child: StoreConnector<AppState, _ViewModel>(
          converter: _ViewModel.fromStore,
          onWillChange: (prevState, nextState) {},
          builder: (context, vm) {
            return Container(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: SafeArea(
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 40),
                        child: Text(
                          'Авторизация',
                          style: TextStyle(
                            fontSize: 32,
                            height: 1.4,
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                      ),
                      SizedBox(height: 32),
                      TextFormField(
                        controller: emailController,
                        decoration: InputDecoration(
                          labelText: 'Введите email',
                        ),
                        keyboardType: TextInputType.emailAddress,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Пожалуйста заполните поле';
                          }
                          if (!RegExp(emailPattern).hasMatch(value)) {
                            return 'Не верный формат почты';
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: 24),
                      TextFormField(
                        controller: passwordController,
                        keyboardType: TextInputType.visiblePassword,
                        obscureText: true,
                        decoration: InputDecoration(
                          labelText: 'Введите пароль',
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Пожалуйста заполните поле';
                          }
                          if (value.length < 6) {
                            return 'Длина пароля не должна быть не менее 6 символов';
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: 32),
                      SizedBox(
                        width: double.infinity,
                        child: ElevatedButton(
                          onPressed: () {
                            if (vm.isSubmitting) {
                              return;
                            }
                            if (_formKey.currentState!.validate()) {
                              vm.onSubmit(
                                email: emailController.text,
                                password: passwordController.text,
                              );
                            }
                          },
                          child: Text(
                            vm.isSubmitting ? 'Загрузка' : 'Отправить',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 16),
                      SizedBox(
                        width: double.infinity,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => AuthenticateRegisterScreen(),
                              ),
                            );
                          },
                          child: Text(
                            'У меня нет аккаунта',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

class _ViewModel {
  final Function onSubmit;
  final bool isSubmitting;
  final String? errorMessage;

  _ViewModel({
    required this.onSubmit,
    required this.isSubmitting,
    required this.errorMessage,
  });

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
      onSubmit: ({
        required String email,
        required String password,
      }) {
        store.dispatch(AuthenticateLoginSubmitAction(
          email: email,
          password: password,
        ));
      },
      isSubmitting: selectAuthenticateIsSubmitting(store.state),
      errorMessage: selectAuthenticateErrorMessage(store.state),
    );
  }
}
