import 'package:pluri/authenticate/authtenticate.dart';
import 'package:pluri/store/store.dart';
import 'package:reselect/reselect.dart';

final selectAuthenticateState = (AppState store) => store.authenticate;

final selectIsAuthenticated = createSelector1<AppState, AuthenticateState, bool>(
  selectAuthenticateState,
  (state) => state.isAuthenticated,
);

final selectAuthenticateIsInitializing = createSelector1<AppState, AuthenticateState, bool>(
  selectAuthenticateState,
  (state) => state.isInitializing,
);

final selectAuthenticateIsSubmitting = createSelector1<AppState, AuthenticateState, bool>(
  selectAuthenticateState,
  (state) => state.isSubmitting,
);

final selectAuthenticateErrorMessage = createSelector1<AppState, AuthenticateState, String?>(
  selectAuthenticateState,
  (state) => state.errorMessage,
);
