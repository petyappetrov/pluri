import 'package:flutter/material.dart';

part 'theme_dark.dart';
part 'theme_light.dart';

final ThemeData theme = ThemeData(
  fontFamily: 'Montserrat',
);
