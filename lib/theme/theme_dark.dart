part of 'theme.dart';

final darkTheme = theme.copyWith(
  brightness: Brightness.dark,
  scaffoldBackgroundColor: Color.fromRGBO(23, 25, 35, 1),
  iconTheme: IconThemeData(
    color: Colors.white,
  ),
  colorScheme: ColorScheme.dark().copyWith(
    secondary: Colors.black,
    primary: Colors.white,
    onPrimary: Color.fromRGBO(26, 32, 44, 1),
  ),
  textTheme: TextTheme(
    bodyText1: TextStyle(color: Colors.white, fontFamily: 'Montserrat'),
    bodyText2: TextStyle(color: Colors.white, fontFamily: 'Montserrat'),
  ),
  inputDecorationTheme: InputDecorationTheme(
    floatingLabelStyle: TextStyle(
      color: Color.fromRGBO(82, 48, 208, 1),
      fontWeight: FontWeight.bold,
      fontFamily: 'Montserrat',
    ),
    labelStyle: TextStyle(
      color: Colors.white.withOpacity(0.6),
      fontWeight: FontWeight.bold,
      fontFamily: 'Montserrat',
    ),
    focusColor: Colors.white,
    contentPadding: EdgeInsets.all(20),
    focusedErrorBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: Color.fromRGBO(82, 48, 208, 1),
        width: 2.0,
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(16),
      ),
    ),
    errorBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: Color.fromRGBO(82, 48, 208, 0.5),
        width: 2.0,
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(16),
      ),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: Color.fromRGBO(82, 48, 208, 1),
        width: 2.0,
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(16),
      ),
    ),
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: Color.fromRGBO(82, 48, 208, 0.5),
        width: 2.0,
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(16),
      ),
    ),
    border: OutlineInputBorder(
      borderSide: BorderSide(
        color: Color.fromRGBO(82, 48, 208, 0.5),
        width: 2.0,
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(16),
      ),
    ),
  ),
  textButtonTheme: TextButtonThemeData(
    style: TextButton.styleFrom(
      primary: Colors.white,
      padding: EdgeInsets.fromLTRB(32, 16, 32, 16),
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(16.0)),
      ),
      elevation: 0,
      textStyle: TextStyle(
        fontFamily: 'Montserrat',
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      primary: Color.fromRGBO(82, 48, 208, 1),
      onPrimary: Colors.white,
      padding: EdgeInsets.fromLTRB(32, 16, 32, 16),
      elevation: 0,
      enableFeedback: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(16.0)),
      ),
      textStyle: TextStyle(
        fontFamily: 'Montserrat',
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
  ),
  outlinedButtonTheme: OutlinedButtonThemeData(
    style: OutlinedButton.styleFrom(
      elevation: 0,
      primary: Colors.white,
      padding: EdgeInsets.fromLTRB(32, 16, 32, 16),
      side: BorderSide(
        color: Colors.white.withOpacity(0.5),
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(16.0)),
      ),
      textStyle: TextStyle(
        fontFamily: 'Montserrat',
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
  ),
);
