part of 'theme.dart';

final lightTheme = theme.copyWith(
  brightness: Brightness.light,
  scaffoldBackgroundColor: Color.fromRGBO(244, 244, 244, 1),
  iconTheme: IconThemeData(
    color: Colors.black,
  ),
  colorScheme: ColorScheme.dark().copyWith(
    secondary: Colors.white,
    primary: Colors.black,
    onPrimary: Colors.white,
  ),
  textTheme: TextTheme(
    subtitle1: TextStyle(color: Colors.black, fontFamily: 'Montserrat'),
    bodyText1: TextStyle(color: Colors.black, fontFamily: 'Montserrat'),
    bodyText2: TextStyle(color: Colors.black, fontFamily: 'Montserrat'),
  ),
  inputDecorationTheme: InputDecorationTheme(
    floatingLabelStyle: TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.bold,
      fontFamily: 'Montserrat',
    ),
    labelStyle: TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.bold,
      fontFamily: 'Montserrat',
    ),
    focusColor: Colors.black,
    contentPadding: EdgeInsets.all(20),
    errorBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: Color.fromRGBO(245, 101, 101, 1),
        width: 2.0,
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(16),
      ),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: Colors.black,
        width: 2.0,
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(16),
      ),
    ),
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: Colors.black,
        width: 2.0,
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(16),
      ),
    ),
    border: OutlineInputBorder(
      borderSide: BorderSide(
        color: Colors.black,
        width: 2.0,
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(16),
      ),
    ),
  ),
  textButtonTheme: TextButtonThemeData(
    style: TextButton.styleFrom(
      primary: Colors.black,
      padding: EdgeInsets.fromLTRB(32, 16, 32, 16),
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(16.0)),
      ),
      elevation: 0,
      textStyle: TextStyle(
        fontFamily: 'Montserrat',
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    ),
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      primary: Color.fromRGBO(213, 63, 140, 1),
      onPrimary: Colors.white,
      padding: EdgeInsets.fromLTRB(32, 16, 32, 16),
      elevation: 0,
      textStyle: TextStyle(
        fontFamily: 'Montserrat',
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
      enableFeedback: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(16.0)),
      ),
    ),
  ),
  outlinedButtonTheme: OutlinedButtonThemeData(
    style: OutlinedButton.styleFrom(
      elevation: 0,
      textStyle: TextStyle(
        fontFamily: 'Montserrat',
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
      primary: Colors.black,
      padding: EdgeInsets.fromLTRB(32, 16, 32, 16),
      side: BorderSide(
        color: Colors.black,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(16.0)),
      ),
    ),
  ),
);
