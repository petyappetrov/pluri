import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';
import 'package:pluri/challenges/challenges.dart';
import 'package:pluri/profile/profile.dart';

part 'activity_entity.freezed.dart';
part 'activity_entity.g.dart';

@freezed
abstract class ActivityEntity with _$ActivityEntity {
  factory ActivityEntity({
    @JsonKey(name: 'id') required int id,
    required DateTime createdDate,
    required ProfileEntity user,
    required ChallengeMissionEntity mission,
    required ChallengeEntity challenge,
    required int countComments,
    required int countLikes,
    String? message,
    // FileEntity? photo,
  }) = _ActivityEntity;

  factory ActivityEntity.fromJson(Map<String, dynamic> json) => _$ActivityEntityFromJson(json);
}
