// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ActivityEntity _$_$_ActivityEntityFromJson(Map<String, dynamic> json) {
  return _$_ActivityEntity(
    id: json['id'] as int,
    createdDate: DateTime.parse(json['createdDate'] as String),
    user: ProfileEntity.fromJson(json['user'] as Map<String, dynamic>),
    mission: ChallengeMissionEntity.fromJson(
        json['mission'] as Map<String, dynamic>),
    challenge:
        ChallengeEntity.fromJson(json['challenge'] as Map<String, dynamic>),
    countComments: json['countComments'] as int,
    countLikes: json['countLikes'] as int,
    message: json['message'] as String?,
  );
}

Map<String, dynamic> _$_$_ActivityEntityToJson(_$_ActivityEntity instance) =>
    <String, dynamic>{
      'id': instance.id,
      'createdDate': instance.createdDate.toIso8601String(),
      'user': instance.user,
      'mission': instance.mission,
      'challenge': instance.challenge,
      'countComments': instance.countComments,
      'countLikes': instance.countLikes,
      'message': instance.message,
    };
