// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'activity_entity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ActivityEntity _$ActivityEntityFromJson(Map<String, dynamic> json) {
  return _ActivityEntity.fromJson(json);
}

/// @nodoc
class _$ActivityEntityTearOff {
  const _$ActivityEntityTearOff();

  _ActivityEntity call(
      {@JsonKey(name: 'id') required int id,
      required DateTime createdDate,
      required ProfileEntity user,
      required ChallengeMissionEntity mission,
      required ChallengeEntity challenge,
      required int countComments,
      required int countLikes,
      String? message}) {
    return _ActivityEntity(
      id: id,
      createdDate: createdDate,
      user: user,
      mission: mission,
      challenge: challenge,
      countComments: countComments,
      countLikes: countLikes,
      message: message,
    );
  }

  ActivityEntity fromJson(Map<String, Object> json) {
    return ActivityEntity.fromJson(json);
  }
}

/// @nodoc
const $ActivityEntity = _$ActivityEntityTearOff();

/// @nodoc
mixin _$ActivityEntity {
  @JsonKey(name: 'id')
  int get id => throw _privateConstructorUsedError;
  DateTime get createdDate => throw _privateConstructorUsedError;
  ProfileEntity get user => throw _privateConstructorUsedError;
  ChallengeMissionEntity get mission => throw _privateConstructorUsedError;
  ChallengeEntity get challenge => throw _privateConstructorUsedError;
  int get countComments => throw _privateConstructorUsedError;
  int get countLikes => throw _privateConstructorUsedError;
  String? get message => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ActivityEntityCopyWith<ActivityEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ActivityEntityCopyWith<$Res> {
  factory $ActivityEntityCopyWith(
          ActivityEntity value, $Res Function(ActivityEntity) then) =
      _$ActivityEntityCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id') int id,
      DateTime createdDate,
      ProfileEntity user,
      ChallengeMissionEntity mission,
      ChallengeEntity challenge,
      int countComments,
      int countLikes,
      String? message});

  $ProfileEntityCopyWith<$Res> get user;
  $ChallengeMissionEntityCopyWith<$Res> get mission;
  $ChallengeEntityCopyWith<$Res> get challenge;
}

/// @nodoc
class _$ActivityEntityCopyWithImpl<$Res>
    implements $ActivityEntityCopyWith<$Res> {
  _$ActivityEntityCopyWithImpl(this._value, this._then);

  final ActivityEntity _value;
  // ignore: unused_field
  final $Res Function(ActivityEntity) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? createdDate = freezed,
    Object? user = freezed,
    Object? mission = freezed,
    Object? challenge = freezed,
    Object? countComments = freezed,
    Object? countLikes = freezed,
    Object? message = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      createdDate: createdDate == freezed
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as ProfileEntity,
      mission: mission == freezed
          ? _value.mission
          : mission // ignore: cast_nullable_to_non_nullable
              as ChallengeMissionEntity,
      challenge: challenge == freezed
          ? _value.challenge
          : challenge // ignore: cast_nullable_to_non_nullable
              as ChallengeEntity,
      countComments: countComments == freezed
          ? _value.countComments
          : countComments // ignore: cast_nullable_to_non_nullable
              as int,
      countLikes: countLikes == freezed
          ? _value.countLikes
          : countLikes // ignore: cast_nullable_to_non_nullable
              as int,
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }

  @override
  $ProfileEntityCopyWith<$Res> get user {
    return $ProfileEntityCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }

  @override
  $ChallengeMissionEntityCopyWith<$Res> get mission {
    return $ChallengeMissionEntityCopyWith<$Res>(_value.mission, (value) {
      return _then(_value.copyWith(mission: value));
    });
  }

  @override
  $ChallengeEntityCopyWith<$Res> get challenge {
    return $ChallengeEntityCopyWith<$Res>(_value.challenge, (value) {
      return _then(_value.copyWith(challenge: value));
    });
  }
}

/// @nodoc
abstract class _$ActivityEntityCopyWith<$Res>
    implements $ActivityEntityCopyWith<$Res> {
  factory _$ActivityEntityCopyWith(
          _ActivityEntity value, $Res Function(_ActivityEntity) then) =
      __$ActivityEntityCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id') int id,
      DateTime createdDate,
      ProfileEntity user,
      ChallengeMissionEntity mission,
      ChallengeEntity challenge,
      int countComments,
      int countLikes,
      String? message});

  @override
  $ProfileEntityCopyWith<$Res> get user;
  @override
  $ChallengeMissionEntityCopyWith<$Res> get mission;
  @override
  $ChallengeEntityCopyWith<$Res> get challenge;
}

/// @nodoc
class __$ActivityEntityCopyWithImpl<$Res>
    extends _$ActivityEntityCopyWithImpl<$Res>
    implements _$ActivityEntityCopyWith<$Res> {
  __$ActivityEntityCopyWithImpl(
      _ActivityEntity _value, $Res Function(_ActivityEntity) _then)
      : super(_value, (v) => _then(v as _ActivityEntity));

  @override
  _ActivityEntity get _value => super._value as _ActivityEntity;

  @override
  $Res call({
    Object? id = freezed,
    Object? createdDate = freezed,
    Object? user = freezed,
    Object? mission = freezed,
    Object? challenge = freezed,
    Object? countComments = freezed,
    Object? countLikes = freezed,
    Object? message = freezed,
  }) {
    return _then(_ActivityEntity(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      createdDate: createdDate == freezed
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as ProfileEntity,
      mission: mission == freezed
          ? _value.mission
          : mission // ignore: cast_nullable_to_non_nullable
              as ChallengeMissionEntity,
      challenge: challenge == freezed
          ? _value.challenge
          : challenge // ignore: cast_nullable_to_non_nullable
              as ChallengeEntity,
      countComments: countComments == freezed
          ? _value.countComments
          : countComments // ignore: cast_nullable_to_non_nullable
              as int,
      countLikes: countLikes == freezed
          ? _value.countLikes
          : countLikes // ignore: cast_nullable_to_non_nullable
              as int,
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ActivityEntity
    with DiagnosticableTreeMixin
    implements _ActivityEntity {
  _$_ActivityEntity(
      {@JsonKey(name: 'id') required this.id,
      required this.createdDate,
      required this.user,
      required this.mission,
      required this.challenge,
      required this.countComments,
      required this.countLikes,
      this.message});

  factory _$_ActivityEntity.fromJson(Map<String, dynamic> json) =>
      _$_$_ActivityEntityFromJson(json);

  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  final DateTime createdDate;
  @override
  final ProfileEntity user;
  @override
  final ChallengeMissionEntity mission;
  @override
  final ChallengeEntity challenge;
  @override
  final int countComments;
  @override
  final int countLikes;
  @override
  final String? message;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ActivityEntity(id: $id, createdDate: $createdDate, user: $user, mission: $mission, challenge: $challenge, countComments: $countComments, countLikes: $countLikes, message: $message)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ActivityEntity'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('createdDate', createdDate))
      ..add(DiagnosticsProperty('user', user))
      ..add(DiagnosticsProperty('mission', mission))
      ..add(DiagnosticsProperty('challenge', challenge))
      ..add(DiagnosticsProperty('countComments', countComments))
      ..add(DiagnosticsProperty('countLikes', countLikes))
      ..add(DiagnosticsProperty('message', message));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ActivityEntity &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.createdDate, createdDate) ||
                const DeepCollectionEquality()
                    .equals(other.createdDate, createdDate)) &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)) &&
            (identical(other.mission, mission) ||
                const DeepCollectionEquality()
                    .equals(other.mission, mission)) &&
            (identical(other.challenge, challenge) ||
                const DeepCollectionEquality()
                    .equals(other.challenge, challenge)) &&
            (identical(other.countComments, countComments) ||
                const DeepCollectionEquality()
                    .equals(other.countComments, countComments)) &&
            (identical(other.countLikes, countLikes) ||
                const DeepCollectionEquality()
                    .equals(other.countLikes, countLikes)) &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(createdDate) ^
      const DeepCollectionEquality().hash(user) ^
      const DeepCollectionEquality().hash(mission) ^
      const DeepCollectionEquality().hash(challenge) ^
      const DeepCollectionEquality().hash(countComments) ^
      const DeepCollectionEquality().hash(countLikes) ^
      const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  _$ActivityEntityCopyWith<_ActivityEntity> get copyWith =>
      __$ActivityEntityCopyWithImpl<_ActivityEntity>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ActivityEntityToJson(this);
  }
}

abstract class _ActivityEntity implements ActivityEntity {
  factory _ActivityEntity(
      {@JsonKey(name: 'id') required int id,
      required DateTime createdDate,
      required ProfileEntity user,
      required ChallengeMissionEntity mission,
      required ChallengeEntity challenge,
      required int countComments,
      required int countLikes,
      String? message}) = _$_ActivityEntity;

  factory _ActivityEntity.fromJson(Map<String, dynamic> json) =
      _$_ActivityEntity.fromJson;

  @override
  @JsonKey(name: 'id')
  int get id => throw _privateConstructorUsedError;
  @override
  DateTime get createdDate => throw _privateConstructorUsedError;
  @override
  ProfileEntity get user => throw _privateConstructorUsedError;
  @override
  ChallengeMissionEntity get mission => throw _privateConstructorUsedError;
  @override
  ChallengeEntity get challenge => throw _privateConstructorUsedError;
  @override
  int get countComments => throw _privateConstructorUsedError;
  @override
  int get countLikes => throw _privateConstructorUsedError;
  @override
  String? get message => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$ActivityEntityCopyWith<_ActivityEntity> get copyWith =>
      throw _privateConstructorUsedError;
}
