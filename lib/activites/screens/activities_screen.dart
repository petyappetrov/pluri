import 'package:flutter/material.dart';
import 'package:pluri/root/root.dart';

class ActivitiesScreen extends StatelessWidget {
  const ActivitiesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TopBarUI(title: 'Активность'),
        ],
      ),
    );
  }
}
