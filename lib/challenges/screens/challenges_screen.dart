import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pluri/root/root.dart';

class Challenge {
  final String title;
  final String image;
  final int color;
  final int difficulty;

  Challenge({
    required this.title,
    required this.image,
    required this.color,
    required this.difficulty,
  });
}

final challenges = [
  Challenge(
    title: 'Новое фото каждый день',
    image: 'assets/images/photo-1.png',
    color: 0xffF4EEE4,
    difficulty: 1,
  ),
  Challenge(
    title: 'Откажись от\u00A0сахара',
    image: 'assets/images/photo-2.png',
    color: 0xffFFEBD8,
    difficulty: 3,
  ),
  Challenge(
    title: 'Новое блюдо каждый день',
    image: 'assets/images/photo-3.png',
    color: 0xffEAF2B9,
    difficulty: 2,
  ),
];

class ChallengesScreen extends StatelessWidget {
  const ChallengesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverAppBar(
          flexibleSpace: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              TopBarUI(title: 'Челленджи')
            ],
          ),
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          toolbarHeight: 100,
          floating: true,
          elevation: 0,
          primary: false,
        ),
        SliverList(
          delegate: SliverChildBuilderDelegate(
            (context, index) {
              return renderChallengeItem(challenge: challenges[index]);
            },
            childCount: challenges.length,
          ),
        )
      ],
    );
  }

  Padding renderChallengeItem({required Challenge challenge}) {
    return Padding(
      padding: EdgeInsets.only(bottom: 16),
      child: BoxUI(
        child: Container(
          width: double.infinity,
          height: 160,
          color: Color(challenge.color),
          child: Stack(
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: Image.asset(challenge.image),
              ),
              Padding(
                padding: EdgeInsets.all(24),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 60),
                      child: Text(
                        challenge.title,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Spacer(),
                    renderDifficulty(challenge.difficulty),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Flex renderDifficulty(int difficulty) {
    return Flex(
      direction: Axis.horizontal,
      children: [
        Text(
          'Сложность',
          style: TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          width: 8,
        ),
        Container(
          width: 44,
          height: 20,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(14),
            color: Colors.white,
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 4.0),
            child: Flex(
              direction: Axis.horizontal,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: List<int>.generate(3, (i) => i + 1)
                  .map(
                    (index) => Container(
                      width: 6,
                      height: 6,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(3),
                        color: difficulty >= index ? Colors.black : Colors.black.withOpacity(0.2),
                      ),
                    ),
                  )
                  .toList(),
            ),
          ),
        )
      ],
    );
  }
}
