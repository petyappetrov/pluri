import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'challenge_mission_entity.freezed.dart';
part 'challenge_mission_entity.g.dart';

@freezed
abstract class ChallengeMissionEntity with _$ChallengeMissionEntity {
  factory ChallengeMissionEntity({
    @JsonKey(name: 'id') required int id,
    required String createdDate,
    required String description,
    required bool isRequiredPhoto,
    required bool isRequiredMessage,
    required bool isRemind,
    required bool isWarn,
  }) = _ChallengeMissionEntity;

  factory ChallengeMissionEntity.fromJson(Map<String, dynamic> json) => _$ChallengeMissionEntityFromJson(json);
}
