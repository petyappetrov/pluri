// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'challenge_entity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ChallengeEntity _$ChallengeEntityFromJson(Map<String, dynamic> json) {
  return _ChallengeEntity.fromJson(json);
}

/// @nodoc
class _$ChallengeEntityTearOff {
  const _$ChallengeEntityTearOff();

  _ChallengeEntity call(
      {@JsonKey(name: 'id') required int id,
      required String createdDate,
      required String name,
      required String description,
      required ChallengeStatusEnum status,
      FileEntity? image,
      List<ChallengeMissionEntity>? missions,
      int? countMembersProgress,
      int? countMembersFinished,
      String? message}) {
    return _ChallengeEntity(
      id: id,
      createdDate: createdDate,
      name: name,
      description: description,
      status: status,
      image: image,
      missions: missions,
      countMembersProgress: countMembersProgress,
      countMembersFinished: countMembersFinished,
      message: message,
    );
  }

  ChallengeEntity fromJson(Map<String, Object> json) {
    return ChallengeEntity.fromJson(json);
  }
}

/// @nodoc
const $ChallengeEntity = _$ChallengeEntityTearOff();

/// @nodoc
mixin _$ChallengeEntity {
  @JsonKey(name: 'id')
  int get id => throw _privateConstructorUsedError;
  String get createdDate => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get description => throw _privateConstructorUsedError;
  ChallengeStatusEnum get status => throw _privateConstructorUsedError;
  FileEntity? get image => throw _privateConstructorUsedError;
  List<ChallengeMissionEntity>? get missions =>
      throw _privateConstructorUsedError;
  int? get countMembersProgress => throw _privateConstructorUsedError;
  int? get countMembersFinished => throw _privateConstructorUsedError;
  String? get message => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ChallengeEntityCopyWith<ChallengeEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChallengeEntityCopyWith<$Res> {
  factory $ChallengeEntityCopyWith(
          ChallengeEntity value, $Res Function(ChallengeEntity) then) =
      _$ChallengeEntityCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id') int id,
      String createdDate,
      String name,
      String description,
      ChallengeStatusEnum status,
      FileEntity? image,
      List<ChallengeMissionEntity>? missions,
      int? countMembersProgress,
      int? countMembersFinished,
      String? message});

  $FileEntityCopyWith<$Res>? get image;
}

/// @nodoc
class _$ChallengeEntityCopyWithImpl<$Res>
    implements $ChallengeEntityCopyWith<$Res> {
  _$ChallengeEntityCopyWithImpl(this._value, this._then);

  final ChallengeEntity _value;
  // ignore: unused_field
  final $Res Function(ChallengeEntity) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? createdDate = freezed,
    Object? name = freezed,
    Object? description = freezed,
    Object? status = freezed,
    Object? image = freezed,
    Object? missions = freezed,
    Object? countMembersProgress = freezed,
    Object? countMembersFinished = freezed,
    Object? message = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      createdDate: createdDate == freezed
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as ChallengeStatusEnum,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as FileEntity?,
      missions: missions == freezed
          ? _value.missions
          : missions // ignore: cast_nullable_to_non_nullable
              as List<ChallengeMissionEntity>?,
      countMembersProgress: countMembersProgress == freezed
          ? _value.countMembersProgress
          : countMembersProgress // ignore: cast_nullable_to_non_nullable
              as int?,
      countMembersFinished: countMembersFinished == freezed
          ? _value.countMembersFinished
          : countMembersFinished // ignore: cast_nullable_to_non_nullable
              as int?,
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }

  @override
  $FileEntityCopyWith<$Res>? get image {
    if (_value.image == null) {
      return null;
    }

    return $FileEntityCopyWith<$Res>(_value.image!, (value) {
      return _then(_value.copyWith(image: value));
    });
  }
}

/// @nodoc
abstract class _$ChallengeEntityCopyWith<$Res>
    implements $ChallengeEntityCopyWith<$Res> {
  factory _$ChallengeEntityCopyWith(
          _ChallengeEntity value, $Res Function(_ChallengeEntity) then) =
      __$ChallengeEntityCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id') int id,
      String createdDate,
      String name,
      String description,
      ChallengeStatusEnum status,
      FileEntity? image,
      List<ChallengeMissionEntity>? missions,
      int? countMembersProgress,
      int? countMembersFinished,
      String? message});

  @override
  $FileEntityCopyWith<$Res>? get image;
}

/// @nodoc
class __$ChallengeEntityCopyWithImpl<$Res>
    extends _$ChallengeEntityCopyWithImpl<$Res>
    implements _$ChallengeEntityCopyWith<$Res> {
  __$ChallengeEntityCopyWithImpl(
      _ChallengeEntity _value, $Res Function(_ChallengeEntity) _then)
      : super(_value, (v) => _then(v as _ChallengeEntity));

  @override
  _ChallengeEntity get _value => super._value as _ChallengeEntity;

  @override
  $Res call({
    Object? id = freezed,
    Object? createdDate = freezed,
    Object? name = freezed,
    Object? description = freezed,
    Object? status = freezed,
    Object? image = freezed,
    Object? missions = freezed,
    Object? countMembersProgress = freezed,
    Object? countMembersFinished = freezed,
    Object? message = freezed,
  }) {
    return _then(_ChallengeEntity(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      createdDate: createdDate == freezed
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as ChallengeStatusEnum,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as FileEntity?,
      missions: missions == freezed
          ? _value.missions
          : missions // ignore: cast_nullable_to_non_nullable
              as List<ChallengeMissionEntity>?,
      countMembersProgress: countMembersProgress == freezed
          ? _value.countMembersProgress
          : countMembersProgress // ignore: cast_nullable_to_non_nullable
              as int?,
      countMembersFinished: countMembersFinished == freezed
          ? _value.countMembersFinished
          : countMembersFinished // ignore: cast_nullable_to_non_nullable
              as int?,
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ChallengeEntity
    with DiagnosticableTreeMixin
    implements _ChallengeEntity {
  _$_ChallengeEntity(
      {@JsonKey(name: 'id') required this.id,
      required this.createdDate,
      required this.name,
      required this.description,
      required this.status,
      this.image,
      this.missions,
      this.countMembersProgress,
      this.countMembersFinished,
      this.message});

  factory _$_ChallengeEntity.fromJson(Map<String, dynamic> json) =>
      _$_$_ChallengeEntityFromJson(json);

  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  final String createdDate;
  @override
  final String name;
  @override
  final String description;
  @override
  final ChallengeStatusEnum status;
  @override
  final FileEntity? image;
  @override
  final List<ChallengeMissionEntity>? missions;
  @override
  final int? countMembersProgress;
  @override
  final int? countMembersFinished;
  @override
  final String? message;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ChallengeEntity(id: $id, createdDate: $createdDate, name: $name, description: $description, status: $status, image: $image, missions: $missions, countMembersProgress: $countMembersProgress, countMembersFinished: $countMembersFinished, message: $message)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ChallengeEntity'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('createdDate', createdDate))
      ..add(DiagnosticsProperty('name', name))
      ..add(DiagnosticsProperty('description', description))
      ..add(DiagnosticsProperty('status', status))
      ..add(DiagnosticsProperty('image', image))
      ..add(DiagnosticsProperty('missions', missions))
      ..add(DiagnosticsProperty('countMembersProgress', countMembersProgress))
      ..add(DiagnosticsProperty('countMembersFinished', countMembersFinished))
      ..add(DiagnosticsProperty('message', message));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ChallengeEntity &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.createdDate, createdDate) ||
                const DeepCollectionEquality()
                    .equals(other.createdDate, createdDate)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)) &&
            (identical(other.status, status) ||
                const DeepCollectionEquality().equals(other.status, status)) &&
            (identical(other.image, image) ||
                const DeepCollectionEquality().equals(other.image, image)) &&
            (identical(other.missions, missions) ||
                const DeepCollectionEquality()
                    .equals(other.missions, missions)) &&
            (identical(other.countMembersProgress, countMembersProgress) ||
                const DeepCollectionEquality().equals(
                    other.countMembersProgress, countMembersProgress)) &&
            (identical(other.countMembersFinished, countMembersFinished) ||
                const DeepCollectionEquality().equals(
                    other.countMembersFinished, countMembersFinished)) &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(createdDate) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(description) ^
      const DeepCollectionEquality().hash(status) ^
      const DeepCollectionEquality().hash(image) ^
      const DeepCollectionEquality().hash(missions) ^
      const DeepCollectionEquality().hash(countMembersProgress) ^
      const DeepCollectionEquality().hash(countMembersFinished) ^
      const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  _$ChallengeEntityCopyWith<_ChallengeEntity> get copyWith =>
      __$ChallengeEntityCopyWithImpl<_ChallengeEntity>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ChallengeEntityToJson(this);
  }
}

abstract class _ChallengeEntity implements ChallengeEntity {
  factory _ChallengeEntity(
      {@JsonKey(name: 'id') required int id,
      required String createdDate,
      required String name,
      required String description,
      required ChallengeStatusEnum status,
      FileEntity? image,
      List<ChallengeMissionEntity>? missions,
      int? countMembersProgress,
      int? countMembersFinished,
      String? message}) = _$_ChallengeEntity;

  factory _ChallengeEntity.fromJson(Map<String, dynamic> json) =
      _$_ChallengeEntity.fromJson;

  @override
  @JsonKey(name: 'id')
  int get id => throw _privateConstructorUsedError;
  @override
  String get createdDate => throw _privateConstructorUsedError;
  @override
  String get name => throw _privateConstructorUsedError;
  @override
  String get description => throw _privateConstructorUsedError;
  @override
  ChallengeStatusEnum get status => throw _privateConstructorUsedError;
  @override
  FileEntity? get image => throw _privateConstructorUsedError;
  @override
  List<ChallengeMissionEntity>? get missions =>
      throw _privateConstructorUsedError;
  @override
  int? get countMembersProgress => throw _privateConstructorUsedError;
  @override
  int? get countMembersFinished => throw _privateConstructorUsedError;
  @override
  String? get message => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$ChallengeEntityCopyWith<_ChallengeEntity> get copyWith =>
      throw _privateConstructorUsedError;
}
