import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';
import 'package:pluri/challenges/challenges.dart';
import 'package:pluri/root/root.dart';

part 'challenge_entity.freezed.dart';
part 'challenge_entity.g.dart';

enum ChallengeStatusEnum {
  NOT_PUBLISHED,
  PUBLISHED,
}

@freezed
abstract class ChallengeEntity with _$ChallengeEntity {
  factory ChallengeEntity({
    @JsonKey(name: 'id') required int id,
    required String createdDate,
    required String name,
    required String description,
    required ChallengeStatusEnum status,
    FileEntity? image,
    List<ChallengeMissionEntity>? missions,
    int? countMembersProgress,
    int? countMembersFinished,
    String? message,
  }) = _ChallengeEntity;

  factory ChallengeEntity.fromJson(Map<String, dynamic> json) => _$ChallengeEntityFromJson(json);
}
