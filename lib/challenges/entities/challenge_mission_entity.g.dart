// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'challenge_mission_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ChallengeMissionEntity _$_$_ChallengeMissionEntityFromJson(
    Map<String, dynamic> json) {
  return _$_ChallengeMissionEntity(
    id: json['id'] as int,
    createdDate: json['createdDate'] as String,
    description: json['description'] as String,
    isRequiredPhoto: json['isRequiredPhoto'] as bool,
    isRequiredMessage: json['isRequiredMessage'] as bool,
    isRemind: json['isRemind'] as bool,
    isWarn: json['isWarn'] as bool,
  );
}

Map<String, dynamic> _$_$_ChallengeMissionEntityToJson(
        _$_ChallengeMissionEntity instance) =>
    <String, dynamic>{
      'id': instance.id,
      'createdDate': instance.createdDate,
      'description': instance.description,
      'isRequiredPhoto': instance.isRequiredPhoto,
      'isRequiredMessage': instance.isRequiredMessage,
      'isRemind': instance.isRemind,
      'isWarn': instance.isWarn,
    };
