// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'challenge_mission_entity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ChallengeMissionEntity _$ChallengeMissionEntityFromJson(
    Map<String, dynamic> json) {
  return _ChallengeMissionEntity.fromJson(json);
}

/// @nodoc
class _$ChallengeMissionEntityTearOff {
  const _$ChallengeMissionEntityTearOff();

  _ChallengeMissionEntity call(
      {@JsonKey(name: 'id') required int id,
      required String createdDate,
      required String description,
      required bool isRequiredPhoto,
      required bool isRequiredMessage,
      required bool isRemind,
      required bool isWarn}) {
    return _ChallengeMissionEntity(
      id: id,
      createdDate: createdDate,
      description: description,
      isRequiredPhoto: isRequiredPhoto,
      isRequiredMessage: isRequiredMessage,
      isRemind: isRemind,
      isWarn: isWarn,
    );
  }

  ChallengeMissionEntity fromJson(Map<String, Object> json) {
    return ChallengeMissionEntity.fromJson(json);
  }
}

/// @nodoc
const $ChallengeMissionEntity = _$ChallengeMissionEntityTearOff();

/// @nodoc
mixin _$ChallengeMissionEntity {
  @JsonKey(name: 'id')
  int get id => throw _privateConstructorUsedError;
  String get createdDate => throw _privateConstructorUsedError;
  String get description => throw _privateConstructorUsedError;
  bool get isRequiredPhoto => throw _privateConstructorUsedError;
  bool get isRequiredMessage => throw _privateConstructorUsedError;
  bool get isRemind => throw _privateConstructorUsedError;
  bool get isWarn => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ChallengeMissionEntityCopyWith<ChallengeMissionEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChallengeMissionEntityCopyWith<$Res> {
  factory $ChallengeMissionEntityCopyWith(ChallengeMissionEntity value,
          $Res Function(ChallengeMissionEntity) then) =
      _$ChallengeMissionEntityCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id') int id,
      String createdDate,
      String description,
      bool isRequiredPhoto,
      bool isRequiredMessage,
      bool isRemind,
      bool isWarn});
}

/// @nodoc
class _$ChallengeMissionEntityCopyWithImpl<$Res>
    implements $ChallengeMissionEntityCopyWith<$Res> {
  _$ChallengeMissionEntityCopyWithImpl(this._value, this._then);

  final ChallengeMissionEntity _value;
  // ignore: unused_field
  final $Res Function(ChallengeMissionEntity) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? createdDate = freezed,
    Object? description = freezed,
    Object? isRequiredPhoto = freezed,
    Object? isRequiredMessage = freezed,
    Object? isRemind = freezed,
    Object? isWarn = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      createdDate: createdDate == freezed
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as String,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      isRequiredPhoto: isRequiredPhoto == freezed
          ? _value.isRequiredPhoto
          : isRequiredPhoto // ignore: cast_nullable_to_non_nullable
              as bool,
      isRequiredMessage: isRequiredMessage == freezed
          ? _value.isRequiredMessage
          : isRequiredMessage // ignore: cast_nullable_to_non_nullable
              as bool,
      isRemind: isRemind == freezed
          ? _value.isRemind
          : isRemind // ignore: cast_nullable_to_non_nullable
              as bool,
      isWarn: isWarn == freezed
          ? _value.isWarn
          : isWarn // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$ChallengeMissionEntityCopyWith<$Res>
    implements $ChallengeMissionEntityCopyWith<$Res> {
  factory _$ChallengeMissionEntityCopyWith(_ChallengeMissionEntity value,
          $Res Function(_ChallengeMissionEntity) then) =
      __$ChallengeMissionEntityCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id') int id,
      String createdDate,
      String description,
      bool isRequiredPhoto,
      bool isRequiredMessage,
      bool isRemind,
      bool isWarn});
}

/// @nodoc
class __$ChallengeMissionEntityCopyWithImpl<$Res>
    extends _$ChallengeMissionEntityCopyWithImpl<$Res>
    implements _$ChallengeMissionEntityCopyWith<$Res> {
  __$ChallengeMissionEntityCopyWithImpl(_ChallengeMissionEntity _value,
      $Res Function(_ChallengeMissionEntity) _then)
      : super(_value, (v) => _then(v as _ChallengeMissionEntity));

  @override
  _ChallengeMissionEntity get _value => super._value as _ChallengeMissionEntity;

  @override
  $Res call({
    Object? id = freezed,
    Object? createdDate = freezed,
    Object? description = freezed,
    Object? isRequiredPhoto = freezed,
    Object? isRequiredMessage = freezed,
    Object? isRemind = freezed,
    Object? isWarn = freezed,
  }) {
    return _then(_ChallengeMissionEntity(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      createdDate: createdDate == freezed
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as String,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      isRequiredPhoto: isRequiredPhoto == freezed
          ? _value.isRequiredPhoto
          : isRequiredPhoto // ignore: cast_nullable_to_non_nullable
              as bool,
      isRequiredMessage: isRequiredMessage == freezed
          ? _value.isRequiredMessage
          : isRequiredMessage // ignore: cast_nullable_to_non_nullable
              as bool,
      isRemind: isRemind == freezed
          ? _value.isRemind
          : isRemind // ignore: cast_nullable_to_non_nullable
              as bool,
      isWarn: isWarn == freezed
          ? _value.isWarn
          : isWarn // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ChallengeMissionEntity
    with DiagnosticableTreeMixin
    implements _ChallengeMissionEntity {
  _$_ChallengeMissionEntity(
      {@JsonKey(name: 'id') required this.id,
      required this.createdDate,
      required this.description,
      required this.isRequiredPhoto,
      required this.isRequiredMessage,
      required this.isRemind,
      required this.isWarn});

  factory _$_ChallengeMissionEntity.fromJson(Map<String, dynamic> json) =>
      _$_$_ChallengeMissionEntityFromJson(json);

  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  final String createdDate;
  @override
  final String description;
  @override
  final bool isRequiredPhoto;
  @override
  final bool isRequiredMessage;
  @override
  final bool isRemind;
  @override
  final bool isWarn;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ChallengeMissionEntity(id: $id, createdDate: $createdDate, description: $description, isRequiredPhoto: $isRequiredPhoto, isRequiredMessage: $isRequiredMessage, isRemind: $isRemind, isWarn: $isWarn)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ChallengeMissionEntity'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('createdDate', createdDate))
      ..add(DiagnosticsProperty('description', description))
      ..add(DiagnosticsProperty('isRequiredPhoto', isRequiredPhoto))
      ..add(DiagnosticsProperty('isRequiredMessage', isRequiredMessage))
      ..add(DiagnosticsProperty('isRemind', isRemind))
      ..add(DiagnosticsProperty('isWarn', isWarn));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ChallengeMissionEntity &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.createdDate, createdDate) ||
                const DeepCollectionEquality()
                    .equals(other.createdDate, createdDate)) &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)) &&
            (identical(other.isRequiredPhoto, isRequiredPhoto) ||
                const DeepCollectionEquality()
                    .equals(other.isRequiredPhoto, isRequiredPhoto)) &&
            (identical(other.isRequiredMessage, isRequiredMessage) ||
                const DeepCollectionEquality()
                    .equals(other.isRequiredMessage, isRequiredMessage)) &&
            (identical(other.isRemind, isRemind) ||
                const DeepCollectionEquality()
                    .equals(other.isRemind, isRemind)) &&
            (identical(other.isWarn, isWarn) ||
                const DeepCollectionEquality().equals(other.isWarn, isWarn)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(createdDate) ^
      const DeepCollectionEquality().hash(description) ^
      const DeepCollectionEquality().hash(isRequiredPhoto) ^
      const DeepCollectionEquality().hash(isRequiredMessage) ^
      const DeepCollectionEquality().hash(isRemind) ^
      const DeepCollectionEquality().hash(isWarn);

  @JsonKey(ignore: true)
  @override
  _$ChallengeMissionEntityCopyWith<_ChallengeMissionEntity> get copyWith =>
      __$ChallengeMissionEntityCopyWithImpl<_ChallengeMissionEntity>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ChallengeMissionEntityToJson(this);
  }
}

abstract class _ChallengeMissionEntity implements ChallengeMissionEntity {
  factory _ChallengeMissionEntity(
      {@JsonKey(name: 'id') required int id,
      required String createdDate,
      required String description,
      required bool isRequiredPhoto,
      required bool isRequiredMessage,
      required bool isRemind,
      required bool isWarn}) = _$_ChallengeMissionEntity;

  factory _ChallengeMissionEntity.fromJson(Map<String, dynamic> json) =
      _$_ChallengeMissionEntity.fromJson;

  @override
  @JsonKey(name: 'id')
  int get id => throw _privateConstructorUsedError;
  @override
  String get createdDate => throw _privateConstructorUsedError;
  @override
  String get description => throw _privateConstructorUsedError;
  @override
  bool get isRequiredPhoto => throw _privateConstructorUsedError;
  @override
  bool get isRequiredMessage => throw _privateConstructorUsedError;
  @override
  bool get isRemind => throw _privateConstructorUsedError;
  @override
  bool get isWarn => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$ChallengeMissionEntityCopyWith<_ChallengeMissionEntity> get copyWith =>
      throw _privateConstructorUsedError;
}
