// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'challenge_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ChallengeEntity _$_$_ChallengeEntityFromJson(Map<String, dynamic> json) {
  return _$_ChallengeEntity(
    id: json['id'] as int,
    createdDate: json['createdDate'] as String,
    name: json['name'] as String,
    description: json['description'] as String,
    status: _$enumDecode(_$ChallengeStatusEnumEnumMap, json['status']),
    image: json['image'] == null
        ? null
        : FileEntity.fromJson(json['image'] as Map<String, dynamic>),
    missions: (json['missions'] as List<dynamic>?)
        ?.map((e) => ChallengeMissionEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    countMembersProgress: json['countMembersProgress'] as int?,
    countMembersFinished: json['countMembersFinished'] as int?,
    message: json['message'] as String?,
  );
}

Map<String, dynamic> _$_$_ChallengeEntityToJson(_$_ChallengeEntity instance) =>
    <String, dynamic>{
      'id': instance.id,
      'createdDate': instance.createdDate,
      'name': instance.name,
      'description': instance.description,
      'status': _$ChallengeStatusEnumEnumMap[instance.status],
      'image': instance.image,
      'missions': instance.missions,
      'countMembersProgress': instance.countMembersProgress,
      'countMembersFinished': instance.countMembersFinished,
      'message': instance.message,
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

const _$ChallengeStatusEnumEnumMap = {
  ChallengeStatusEnum.NOT_PUBLISHED: 'NOT_PUBLISHED',
  ChallengeStatusEnum.PUBLISHED: 'PUBLISHED',
};
