import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:pluri/authenticate/authtenticate.dart';
import 'package:pluri/home/home.dart';
import 'package:pluri/profile/profile.dart';
import 'package:pluri/root/root.dart';

part 'app_state.freezed.dart';
part 'app_state.g.dart';

@freezed
abstract class AppState with _$AppState {
  factory AppState({
    required RootState root,
    required AuthenticateState authenticate,
    required HomeState home,
    required ProfileState profile,
  }) = _AppState;

  factory AppState.fromJson(
    Map<String, dynamic> json,
  ) =>
      _$AppStateFromJson(json);
}
