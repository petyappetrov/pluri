import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:pluri/authenticate/authtenticate.dart';
import 'package:pluri/home/home.dart';
import 'package:pluri/profile/profile.dart';
import 'package:pluri/profile/profile_saga.dart';
// import 'package:pluri/root/root_reducer.dart';
import 'package:pluri/root/root.dart';
import 'package:pluri/store/store.dart';
import 'package:redux_dev_tools/redux_dev_tools.dart';
// import 'package:redux_logging/redux_logging.dart';
// import 'package:redux_remote_devtools/redux_remote_devtools.dart';
import 'package:redux_saga/redux_saga.dart';
import 'package:redux/redux.dart';

AppState applicationReducer(AppState state, action) => AppState(
      root: rootReducer(state.root, action),
      authenticate: authenticateReducer(state.authenticate, action),
      home: homeReducer(state.home, action),
      profile: profileReducer(state.profile, action),
    );

Future<Store<AppState>> configureStore() async {
  final sagaMiddleware = createSagaMiddleware();

  final initialState = AppState(
    root: await RootState.getPersistedState(),
    authenticate: AuthenticateState(),
    home: HomeState(),
    profile: ProfileState(),
  );

  // final remoteDevtools = RemoteDevToolsMiddleware('192.168.1.147:8000');
  // await remoteDevtools.connect();

  final store = DevToolsStore<AppState>(
    applicationReducer,
    initialState: initialState,
    middleware: [
      // remoteDevtools,
      NavigationMiddleware<AppState>(),
      applyMiddleware(sagaMiddleware),
    ],
  );

  sagaMiddleware.setStore(store);

  rootSaga() sync* {
    yield All(
      {
        #root1: Fork(watchRootInitialize),
        #root2: Fork(watchRootChangeThemeMode),
        #auth1: Fork(watchAuthenticateInitialize),
        #auth2: Fork(watchAuthenticateLogout),
        #auth3: Fork(watchAuthenticateRegister),
        #auth4: Fork(watchAuthentcateSuccessRegister),
        #auth5: Fork(watchAuthenticateLogin),
        #profile1: Fork(watchProfileLoad),
        #profile2: Fork(watchProfileActivitiesLoad),
      },
    );
  }

  sagaMiddleware.run(rootSaga);

  // remoteDevtools.store = store;

  return store;
}
