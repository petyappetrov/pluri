// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AppState _$_$_AppStateFromJson(Map<String, dynamic> json) {
  return _$_AppState(
    root: RootState.fromJson(json['root'] as Map<String, dynamic>),
    authenticate: AuthenticateState.fromJson(
        json['authenticate'] as Map<String, dynamic>),
    home: HomeState.fromJson(json['home'] as Map<String, dynamic>),
    profile: ProfileState.fromJson(json['profile'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$_$_AppStateToJson(_$_AppState instance) =>
    <String, dynamic>{
      'root': instance.root,
      'authenticate': instance.authenticate,
      'home': instance.home,
      'profile': instance.profile,
    };
