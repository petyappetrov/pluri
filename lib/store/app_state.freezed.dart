// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'app_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AppState _$AppStateFromJson(Map<String, dynamic> json) {
  return _AppState.fromJson(json);
}

/// @nodoc
class _$AppStateTearOff {
  const _$AppStateTearOff();

  _AppState call(
      {required RootState root,
      required AuthenticateState authenticate,
      required HomeState home,
      required ProfileState profile}) {
    return _AppState(
      root: root,
      authenticate: authenticate,
      home: home,
      profile: profile,
    );
  }

  AppState fromJson(Map<String, Object> json) {
    return AppState.fromJson(json);
  }
}

/// @nodoc
const $AppState = _$AppStateTearOff();

/// @nodoc
mixin _$AppState {
  RootState get root => throw _privateConstructorUsedError;
  AuthenticateState get authenticate => throw _privateConstructorUsedError;
  HomeState get home => throw _privateConstructorUsedError;
  ProfileState get profile => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AppStateCopyWith<AppState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppStateCopyWith<$Res> {
  factory $AppStateCopyWith(AppState value, $Res Function(AppState) then) =
      _$AppStateCopyWithImpl<$Res>;
  $Res call(
      {RootState root,
      AuthenticateState authenticate,
      HomeState home,
      ProfileState profile});

  $RootStateCopyWith<$Res> get root;
  $AuthenticateStateCopyWith<$Res> get authenticate;
  $HomeStateCopyWith<$Res> get home;
  $ProfileStateCopyWith<$Res> get profile;
}

/// @nodoc
class _$AppStateCopyWithImpl<$Res> implements $AppStateCopyWith<$Res> {
  _$AppStateCopyWithImpl(this._value, this._then);

  final AppState _value;
  // ignore: unused_field
  final $Res Function(AppState) _then;

  @override
  $Res call({
    Object? root = freezed,
    Object? authenticate = freezed,
    Object? home = freezed,
    Object? profile = freezed,
  }) {
    return _then(_value.copyWith(
      root: root == freezed
          ? _value.root
          : root // ignore: cast_nullable_to_non_nullable
              as RootState,
      authenticate: authenticate == freezed
          ? _value.authenticate
          : authenticate // ignore: cast_nullable_to_non_nullable
              as AuthenticateState,
      home: home == freezed
          ? _value.home
          : home // ignore: cast_nullable_to_non_nullable
              as HomeState,
      profile: profile == freezed
          ? _value.profile
          : profile // ignore: cast_nullable_to_non_nullable
              as ProfileState,
    ));
  }

  @override
  $RootStateCopyWith<$Res> get root {
    return $RootStateCopyWith<$Res>(_value.root, (value) {
      return _then(_value.copyWith(root: value));
    });
  }

  @override
  $AuthenticateStateCopyWith<$Res> get authenticate {
    return $AuthenticateStateCopyWith<$Res>(_value.authenticate, (value) {
      return _then(_value.copyWith(authenticate: value));
    });
  }

  @override
  $HomeStateCopyWith<$Res> get home {
    return $HomeStateCopyWith<$Res>(_value.home, (value) {
      return _then(_value.copyWith(home: value));
    });
  }

  @override
  $ProfileStateCopyWith<$Res> get profile {
    return $ProfileStateCopyWith<$Res>(_value.profile, (value) {
      return _then(_value.copyWith(profile: value));
    });
  }
}

/// @nodoc
abstract class _$AppStateCopyWith<$Res> implements $AppStateCopyWith<$Res> {
  factory _$AppStateCopyWith(_AppState value, $Res Function(_AppState) then) =
      __$AppStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {RootState root,
      AuthenticateState authenticate,
      HomeState home,
      ProfileState profile});

  @override
  $RootStateCopyWith<$Res> get root;
  @override
  $AuthenticateStateCopyWith<$Res> get authenticate;
  @override
  $HomeStateCopyWith<$Res> get home;
  @override
  $ProfileStateCopyWith<$Res> get profile;
}

/// @nodoc
class __$AppStateCopyWithImpl<$Res> extends _$AppStateCopyWithImpl<$Res>
    implements _$AppStateCopyWith<$Res> {
  __$AppStateCopyWithImpl(_AppState _value, $Res Function(_AppState) _then)
      : super(_value, (v) => _then(v as _AppState));

  @override
  _AppState get _value => super._value as _AppState;

  @override
  $Res call({
    Object? root = freezed,
    Object? authenticate = freezed,
    Object? home = freezed,
    Object? profile = freezed,
  }) {
    return _then(_AppState(
      root: root == freezed
          ? _value.root
          : root // ignore: cast_nullable_to_non_nullable
              as RootState,
      authenticate: authenticate == freezed
          ? _value.authenticate
          : authenticate // ignore: cast_nullable_to_non_nullable
              as AuthenticateState,
      home: home == freezed
          ? _value.home
          : home // ignore: cast_nullable_to_non_nullable
              as HomeState,
      profile: profile == freezed
          ? _value.profile
          : profile // ignore: cast_nullable_to_non_nullable
              as ProfileState,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AppState implements _AppState {
  _$_AppState(
      {required this.root,
      required this.authenticate,
      required this.home,
      required this.profile});

  factory _$_AppState.fromJson(Map<String, dynamic> json) =>
      _$_$_AppStateFromJson(json);

  @override
  final RootState root;
  @override
  final AuthenticateState authenticate;
  @override
  final HomeState home;
  @override
  final ProfileState profile;

  @override
  String toString() {
    return 'AppState(root: $root, authenticate: $authenticate, home: $home, profile: $profile)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _AppState &&
            (identical(other.root, root) ||
                const DeepCollectionEquality().equals(other.root, root)) &&
            (identical(other.authenticate, authenticate) ||
                const DeepCollectionEquality()
                    .equals(other.authenticate, authenticate)) &&
            (identical(other.home, home) ||
                const DeepCollectionEquality().equals(other.home, home)) &&
            (identical(other.profile, profile) ||
                const DeepCollectionEquality().equals(other.profile, profile)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(root) ^
      const DeepCollectionEquality().hash(authenticate) ^
      const DeepCollectionEquality().hash(home) ^
      const DeepCollectionEquality().hash(profile);

  @JsonKey(ignore: true)
  @override
  _$AppStateCopyWith<_AppState> get copyWith =>
      __$AppStateCopyWithImpl<_AppState>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_AppStateToJson(this);
  }
}

abstract class _AppState implements AppState {
  factory _AppState(
      {required RootState root,
      required AuthenticateState authenticate,
      required HomeState home,
      required ProfileState profile}) = _$_AppState;

  factory _AppState.fromJson(Map<String, dynamic> json) = _$_AppState.fromJson;

  @override
  RootState get root => throw _privateConstructorUsedError;
  @override
  AuthenticateState get authenticate => throw _privateConstructorUsedError;
  @override
  HomeState get home => throw _privateConstructorUsedError;
  @override
  ProfileState get profile => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$AppStateCopyWith<_AppState> get copyWith =>
      throw _privateConstructorUsedError;
}
