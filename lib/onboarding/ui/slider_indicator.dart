import 'dart:math';
import 'package:flutter/material.dart';

class SliderIndicator extends AnimatedWidget {
  SliderIndicator({
    required this.controller,
    required this.itemCount,
  }) : super(listenable: controller);

  final PageController controller;
  final int itemCount;

  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List<Widget>.generate(itemCount, (int index) {
        double selectedness = Curves.linear.transform(
          max(
            0.0,
            1.0 - ((controller.page ?? controller.initialPage) - index).abs(),
          ),
        );
        return Container(
          width: 16.0,
          child: Center(
            child: Material(
              color: Theme.of(context).colorScheme.primary.withOpacity(max(selectedness, 0.3)),
              type: MaterialType.circle,
              child: Container(
                width: 6.0,
                height: 6.0,
              ),
            ),
          ),
        );
      }),
    );
  }
}
