import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class OnboardingSlideFirstScreen extends StatelessWidget {
  const OnboardingSlideFirstScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Center(
              child: Padding(
                padding: EdgeInsets.only(top: 120),
                child: Align(
                  alignment: Alignment.center,
                  child: SvgPicture.asset('assets/images/pixeltrue-meditation-1.svg', semanticsLabel: 'Acme Logo', width: 400,),
                ),
                // child: Text(
                //   '🏃‍♂️',
                //   style: TextStyle(fontSize: 82),
                // ),
              ),
            ),
          ),
          Column(
            children: [
              Text(
                'Становись лучше',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 32,
                  height: 1.4,
                  fontWeight: FontWeight.w900,
                ),
              ),
              SizedBox(height: 24),
              Text(
                'Выбери подходящий челлендж и\u00A0выполняй задания каждый день',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                  height: 1.4,
                ),
              ),
              SizedBox(
                height: 190,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
