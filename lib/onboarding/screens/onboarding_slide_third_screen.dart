import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class OnboardingSlideThirdScreen extends StatelessWidget {
  const OnboardingSlideThirdScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Center(
              child: Padding(
                padding: EdgeInsets.only(top: 120),
                child: Align(
                  alignment: Alignment.center,
                  child: SvgPicture.asset(
                    'assets/images/pixeltrue-chatting-using-phone.svg',
                    semanticsLabel: 'Acme Logo',
                    width: 400,
                  ),
                ),
              ),
            ),
          ),
          Column(
            children: [
              Text(
                'Делись своими победами',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 32,
                  height: 1.4,
                  fontWeight: FontWeight.w900,
                ),
              ),
              SizedBox(height: 24),
              Text(
                'Пусть друзья знают как ты хорошо справляешься с заданиями',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                  height: 1.4,
                ),
              ),
              SizedBox(
                height: 190,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
