import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pluri/authenticate/screens/screens.dart';
import 'package:pluri/onboarding/ui/ui.dart';

import 'screens.dart';

class OnboardingScreen extends StatelessWidget {
  OnboardingScreen({Key? key}) : super(key: key);

  final pageController = PageController();

  @override
  Widget build(BuildContext context) {
    final slides = [
      OnboardingSlideFirstScreen(),
      OnboardingSlideSecondScreen(),
      OnboardingSlideThirdScreen(),
      OnboardingSlideFourthScreen(),
    ];
    return Scaffold(
      body: Stack(
        children: <Widget>[
          PageView(
            controller: pageController,
            children: slides,
          ),
          Positioned(
            bottom: 48.0,
            left: 32.0,
            right: 32.0,
            child: Column(
              children: [
                SliderIndicator(
                  controller: pageController,
                  itemCount: slides.length,
                ),
                SizedBox(height: 44),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => AuthenticateRegisterScreen()),
                        );
                      },
                      child: Text(
                        'Пропустить',
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        if (pageController.page == slides.length - 1) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => AuthenticateRegisterScreen()),
                          );
                        } else {
                          pageController.nextPage(
                            duration: Duration(milliseconds: 330),
                            curve: Curves.linearToEaseOut,
                          );
                        }
                      },
                      child: Icon(
                        FontAwesomeIcons.circleChevronRight,
                        size: 24,
                      ),
                    )
                  ],
                ),
                SizedBox(height: 16),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
