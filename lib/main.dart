import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:intl/intl.dart';
import 'package:pluri/http_client.dart';
import 'package:pluri/root/root.dart';
import 'package:redux/redux.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'store/store.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Intl.defaultLocale = 'ru';

  dio.interceptors.add(LogInterceptor());

  dio.interceptors.add(
    InterceptorsWrapper(
      onRequest: (options, handler) async {
        dio.interceptors.requestLock.lock();

        SharedPreferences prefs = await SharedPreferences
          .getInstance();

        final String token = prefs.getString('access_token') ?? '';

        if (token.isNotEmpty) {
          options.headers["Authorization"] = 'Bearer $token';
        }

        dio.interceptors.requestLock.unlock();
        return handler.next(options);
      }
    )
  );
  Store<AppState> store = await configureStore();

  return runApp(Main(store: store));
}

class Main extends StatelessWidget {
  final Store<AppState> store;
  const Main({Key? key, required this.store}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: RootScreen()
    );
  }
}
