import 'package:pluri/activites/activities.dart';
import 'package:pluri/profile/profile.dart';
import 'package:redux_saga/redux_saga.dart';

loadProfile({required ProfileLoadAction action}) sync* {
  yield Try(() sync* {
    final profile = Result<ProfileEntity>();

    yield Call(ProfileService.getProfile, result: profile);

    yield Put(ProfileActivitiesLoadAction());

    yield Take(pattern: ProfileActivitiesLoadSuccessAction);

    yield Put(ProfileLoadSuccessAction(profile: profile.value!));
  }, Catch: (error) sync* {
    yield Put(ProfileLoadFailureAction(errorMessage: 'Серверная ошибка'));
    action.completer!.complete();
  });

  if (action.completer != null) {
    action.completer!.complete();
  }
}

loadProfileActivities({required ProfileActivitiesLoadAction action}) sync* {
  yield Try(() sync* {
    final activities = Result<List<ActivityEntity>>();

    yield Call(ProfileService.getProfileActivities, result: activities);

    yield Put(ProfileActivitiesLoadSuccessAction(activities: activities.value ?? []));
  }, Catch: (error) sync* {
    yield Put(ProfileActivitiesLoadFailureAction(errorMessage: 'Серверная ошибка'));
  });
}

watchProfileLoad({dynamic action}) sync* {
  yield TakeLatest(loadProfile, pattern: ProfileLoadAction);
}

watchProfileActivitiesLoad({dynamic action}) sync* {
  yield TakeLatest(loadProfileActivities, pattern: ProfileActivitiesLoadAction);
}
