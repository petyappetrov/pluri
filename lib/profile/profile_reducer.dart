import 'package:pluri/profile/profile.dart';

ProfileState profileReducer(ProfileState state, action) {
  if (action is ProfileLoadAction) {
    return state.copyWith(
      isLoading: true,
    );
  }
  if (action is ProfileLoadSuccessAction) {
    print(action.profile);
    return state.copyWith(
      isLoading: false,
      profile: action.profile.copyWith(avatar: action.profile.avatar),
    );
  }
  if (action is ProfileLoadFailureAction) {
    return state.copyWith(
      isLoading: false,
      errorMessage: action.errorMessage,
    );
  }
  if (action is ProfileActivitiesLoadAction) {
    return state.copyWith(
      isLoadingActivities: true,
    );
  }
  if (action is ProfileActivitiesLoadSuccessAction) {
    return state.copyWith(
      isLoadingActivities: false,
      activities: action.activities,
    );
  }
  if (action is ProfileActivitiesLoadFailureAction) {
    return state.copyWith(
      isLoadingActivities: false,
      errorMessage: action.errorMessage,
    );
  }
  if (action is ProfileChangeFirebaseTokenSuccessAction) {
    return state.copyWith.profile!(
      firebaseToken: action.firebaseToken,
    );
  }
  if (action is ProfileRemoveFirebaseTokenSuccessAction) {
    return state.copyWith.profile!(
      firebaseToken: null,
    );
  }
  return state;
}
