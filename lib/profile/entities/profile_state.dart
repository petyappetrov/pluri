import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:pluri/activites/activities.dart';
import 'package:pluri/profile/entities/profile_entity.dart';

part 'profile_state.freezed.dart';
part 'profile_state.g.dart';

@freezed
abstract class ProfileState with _$ProfileState {
  factory ProfileState({
    @Default(false) bool isLoading,
    @Default(false) bool isLoadingActivities,
    String? errorMessage,
    ProfileEntity? profile,
    List<ActivityEntity>? activities,
  }) = _ProfileState;

  factory ProfileState.fromJson(Map<String, dynamic> json) => _$ProfileStateFromJson(json);
}
