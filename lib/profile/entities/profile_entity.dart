import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';
import 'package:pluri/root/root.dart';

part 'profile_entity.freezed.dart';
part 'profile_entity.g.dart';

@freezed
abstract class ProfileEntity with _$ProfileEntity {
  factory ProfileEntity({
    @JsonKey(name: 'id') required int id,
    required String email,
    required String name,
    int? countFollowers,
    int? countFollowing,
    int? countMembers,
    int? countActivities,
    String? timezone,
    String? firebaseToken,
    FileEntity? avatar,
  }) = _ProfileEntity;

  factory ProfileEntity.fromJson(Map<String, dynamic> json) => _$ProfileEntityFromJson(json);
}
