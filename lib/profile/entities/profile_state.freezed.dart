// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'profile_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ProfileState _$ProfileStateFromJson(Map<String, dynamic> json) {
  return _ProfileState.fromJson(json);
}

/// @nodoc
class _$ProfileStateTearOff {
  const _$ProfileStateTearOff();

  _ProfileState call(
      {bool isLoading = false,
      bool isLoadingActivities = false,
      String? errorMessage,
      ProfileEntity? profile,
      List<ActivityEntity>? activities}) {
    return _ProfileState(
      isLoading: isLoading,
      isLoadingActivities: isLoadingActivities,
      errorMessage: errorMessage,
      profile: profile,
      activities: activities,
    );
  }

  ProfileState fromJson(Map<String, Object> json) {
    return ProfileState.fromJson(json);
  }
}

/// @nodoc
const $ProfileState = _$ProfileStateTearOff();

/// @nodoc
mixin _$ProfileState {
  bool get isLoading => throw _privateConstructorUsedError;
  bool get isLoadingActivities => throw _privateConstructorUsedError;
  String? get errorMessage => throw _privateConstructorUsedError;
  ProfileEntity? get profile => throw _privateConstructorUsedError;
  List<ActivityEntity>? get activities => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ProfileStateCopyWith<ProfileState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProfileStateCopyWith<$Res> {
  factory $ProfileStateCopyWith(
          ProfileState value, $Res Function(ProfileState) then) =
      _$ProfileStateCopyWithImpl<$Res>;
  $Res call(
      {bool isLoading,
      bool isLoadingActivities,
      String? errorMessage,
      ProfileEntity? profile,
      List<ActivityEntity>? activities});

  $ProfileEntityCopyWith<$Res>? get profile;
}

/// @nodoc
class _$ProfileStateCopyWithImpl<$Res> implements $ProfileStateCopyWith<$Res> {
  _$ProfileStateCopyWithImpl(this._value, this._then);

  final ProfileState _value;
  // ignore: unused_field
  final $Res Function(ProfileState) _then;

  @override
  $Res call({
    Object? isLoading = freezed,
    Object? isLoadingActivities = freezed,
    Object? errorMessage = freezed,
    Object? profile = freezed,
    Object? activities = freezed,
  }) {
    return _then(_value.copyWith(
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadingActivities: isLoadingActivities == freezed
          ? _value.isLoadingActivities
          : isLoadingActivities // ignore: cast_nullable_to_non_nullable
              as bool,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String?,
      profile: profile == freezed
          ? _value.profile
          : profile // ignore: cast_nullable_to_non_nullable
              as ProfileEntity?,
      activities: activities == freezed
          ? _value.activities
          : activities // ignore: cast_nullable_to_non_nullable
              as List<ActivityEntity>?,
    ));
  }

  @override
  $ProfileEntityCopyWith<$Res>? get profile {
    if (_value.profile == null) {
      return null;
    }

    return $ProfileEntityCopyWith<$Res>(_value.profile!, (value) {
      return _then(_value.copyWith(profile: value));
    });
  }
}

/// @nodoc
abstract class _$ProfileStateCopyWith<$Res>
    implements $ProfileStateCopyWith<$Res> {
  factory _$ProfileStateCopyWith(
          _ProfileState value, $Res Function(_ProfileState) then) =
      __$ProfileStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool isLoading,
      bool isLoadingActivities,
      String? errorMessage,
      ProfileEntity? profile,
      List<ActivityEntity>? activities});

  @override
  $ProfileEntityCopyWith<$Res>? get profile;
}

/// @nodoc
class __$ProfileStateCopyWithImpl<$Res> extends _$ProfileStateCopyWithImpl<$Res>
    implements _$ProfileStateCopyWith<$Res> {
  __$ProfileStateCopyWithImpl(
      _ProfileState _value, $Res Function(_ProfileState) _then)
      : super(_value, (v) => _then(v as _ProfileState));

  @override
  _ProfileState get _value => super._value as _ProfileState;

  @override
  $Res call({
    Object? isLoading = freezed,
    Object? isLoadingActivities = freezed,
    Object? errorMessage = freezed,
    Object? profile = freezed,
    Object? activities = freezed,
  }) {
    return _then(_ProfileState(
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadingActivities: isLoadingActivities == freezed
          ? _value.isLoadingActivities
          : isLoadingActivities // ignore: cast_nullable_to_non_nullable
              as bool,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String?,
      profile: profile == freezed
          ? _value.profile
          : profile // ignore: cast_nullable_to_non_nullable
              as ProfileEntity?,
      activities: activities == freezed
          ? _value.activities
          : activities // ignore: cast_nullable_to_non_nullable
              as List<ActivityEntity>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ProfileState implements _ProfileState {
  _$_ProfileState(
      {this.isLoading = false,
      this.isLoadingActivities = false,
      this.errorMessage,
      this.profile,
      this.activities});

  factory _$_ProfileState.fromJson(Map<String, dynamic> json) =>
      _$_$_ProfileStateFromJson(json);

  @JsonKey(defaultValue: false)
  @override
  final bool isLoading;
  @JsonKey(defaultValue: false)
  @override
  final bool isLoadingActivities;
  @override
  final String? errorMessage;
  @override
  final ProfileEntity? profile;
  @override
  final List<ActivityEntity>? activities;

  @override
  String toString() {
    return 'ProfileState(isLoading: $isLoading, isLoadingActivities: $isLoadingActivities, errorMessage: $errorMessage, profile: $profile, activities: $activities)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ProfileState &&
            (identical(other.isLoading, isLoading) ||
                const DeepCollectionEquality()
                    .equals(other.isLoading, isLoading)) &&
            (identical(other.isLoadingActivities, isLoadingActivities) ||
                const DeepCollectionEquality()
                    .equals(other.isLoadingActivities, isLoadingActivities)) &&
            (identical(other.errorMessage, errorMessage) ||
                const DeepCollectionEquality()
                    .equals(other.errorMessage, errorMessage)) &&
            (identical(other.profile, profile) ||
                const DeepCollectionEquality()
                    .equals(other.profile, profile)) &&
            (identical(other.activities, activities) ||
                const DeepCollectionEquality()
                    .equals(other.activities, activities)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(isLoading) ^
      const DeepCollectionEquality().hash(isLoadingActivities) ^
      const DeepCollectionEquality().hash(errorMessage) ^
      const DeepCollectionEquality().hash(profile) ^
      const DeepCollectionEquality().hash(activities);

  @JsonKey(ignore: true)
  @override
  _$ProfileStateCopyWith<_ProfileState> get copyWith =>
      __$ProfileStateCopyWithImpl<_ProfileState>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ProfileStateToJson(this);
  }
}

abstract class _ProfileState implements ProfileState {
  factory _ProfileState(
      {bool isLoading,
      bool isLoadingActivities,
      String? errorMessage,
      ProfileEntity? profile,
      List<ActivityEntity>? activities}) = _$_ProfileState;

  factory _ProfileState.fromJson(Map<String, dynamic> json) =
      _$_ProfileState.fromJson;

  @override
  bool get isLoading => throw _privateConstructorUsedError;
  @override
  bool get isLoadingActivities => throw _privateConstructorUsedError;
  @override
  String? get errorMessage => throw _privateConstructorUsedError;
  @override
  ProfileEntity? get profile => throw _privateConstructorUsedError;
  @override
  List<ActivityEntity>? get activities => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$ProfileStateCopyWith<_ProfileState> get copyWith =>
      throw _privateConstructorUsedError;
}
