// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile_state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ProfileState _$_$_ProfileStateFromJson(Map<String, dynamic> json) {
  return _$_ProfileState(
    isLoading: json['isLoading'] as bool? ?? false,
    isLoadingActivities: json['isLoadingActivities'] as bool? ?? false,
    errorMessage: json['errorMessage'] as String?,
    profile: json['profile'] == null
        ? null
        : ProfileEntity.fromJson(json['profile'] as Map<String, dynamic>),
    activities: (json['activities'] as List<dynamic>?)
        ?.map((e) => ActivityEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$_$_ProfileStateToJson(_$_ProfileState instance) =>
    <String, dynamic>{
      'isLoading': instance.isLoading,
      'isLoadingActivities': instance.isLoadingActivities,
      'errorMessage': instance.errorMessage,
      'profile': instance.profile,
      'activities': instance.activities,
    };
