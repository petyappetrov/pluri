// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'profile_entity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ProfileEntity _$ProfileEntityFromJson(Map<String, dynamic> json) {
  return _ProfileEntity.fromJson(json);
}

/// @nodoc
class _$ProfileEntityTearOff {
  const _$ProfileEntityTearOff();

  _ProfileEntity call(
      {@JsonKey(name: 'id') required int id,
      required String email,
      required String name,
      int? countFollowers,
      int? countFollowing,
      int? countMembers,
      int? countActivities,
      String? timezone,
      String? firebaseToken,
      FileEntity? avatar}) {
    return _ProfileEntity(
      id: id,
      email: email,
      name: name,
      countFollowers: countFollowers,
      countFollowing: countFollowing,
      countMembers: countMembers,
      countActivities: countActivities,
      timezone: timezone,
      firebaseToken: firebaseToken,
      avatar: avatar,
    );
  }

  ProfileEntity fromJson(Map<String, Object> json) {
    return ProfileEntity.fromJson(json);
  }
}

/// @nodoc
const $ProfileEntity = _$ProfileEntityTearOff();

/// @nodoc
mixin _$ProfileEntity {
  @JsonKey(name: 'id')
  int get id => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  int? get countFollowers => throw _privateConstructorUsedError;
  int? get countFollowing => throw _privateConstructorUsedError;
  int? get countMembers => throw _privateConstructorUsedError;
  int? get countActivities => throw _privateConstructorUsedError;
  String? get timezone => throw _privateConstructorUsedError;
  String? get firebaseToken => throw _privateConstructorUsedError;
  FileEntity? get avatar => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ProfileEntityCopyWith<ProfileEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProfileEntityCopyWith<$Res> {
  factory $ProfileEntityCopyWith(
          ProfileEntity value, $Res Function(ProfileEntity) then) =
      _$ProfileEntityCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id') int id,
      String email,
      String name,
      int? countFollowers,
      int? countFollowing,
      int? countMembers,
      int? countActivities,
      String? timezone,
      String? firebaseToken,
      FileEntity? avatar});

  $FileEntityCopyWith<$Res>? get avatar;
}

/// @nodoc
class _$ProfileEntityCopyWithImpl<$Res>
    implements $ProfileEntityCopyWith<$Res> {
  _$ProfileEntityCopyWithImpl(this._value, this._then);

  final ProfileEntity _value;
  // ignore: unused_field
  final $Res Function(ProfileEntity) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? email = freezed,
    Object? name = freezed,
    Object? countFollowers = freezed,
    Object? countFollowing = freezed,
    Object? countMembers = freezed,
    Object? countActivities = freezed,
    Object? timezone = freezed,
    Object? firebaseToken = freezed,
    Object? avatar = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      countFollowers: countFollowers == freezed
          ? _value.countFollowers
          : countFollowers // ignore: cast_nullable_to_non_nullable
              as int?,
      countFollowing: countFollowing == freezed
          ? _value.countFollowing
          : countFollowing // ignore: cast_nullable_to_non_nullable
              as int?,
      countMembers: countMembers == freezed
          ? _value.countMembers
          : countMembers // ignore: cast_nullable_to_non_nullable
              as int?,
      countActivities: countActivities == freezed
          ? _value.countActivities
          : countActivities // ignore: cast_nullable_to_non_nullable
              as int?,
      timezone: timezone == freezed
          ? _value.timezone
          : timezone // ignore: cast_nullable_to_non_nullable
              as String?,
      firebaseToken: firebaseToken == freezed
          ? _value.firebaseToken
          : firebaseToken // ignore: cast_nullable_to_non_nullable
              as String?,
      avatar: avatar == freezed
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as FileEntity?,
    ));
  }

  @override
  $FileEntityCopyWith<$Res>? get avatar {
    if (_value.avatar == null) {
      return null;
    }

    return $FileEntityCopyWith<$Res>(_value.avatar!, (value) {
      return _then(_value.copyWith(avatar: value));
    });
  }
}

/// @nodoc
abstract class _$ProfileEntityCopyWith<$Res>
    implements $ProfileEntityCopyWith<$Res> {
  factory _$ProfileEntityCopyWith(
          _ProfileEntity value, $Res Function(_ProfileEntity) then) =
      __$ProfileEntityCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id') int id,
      String email,
      String name,
      int? countFollowers,
      int? countFollowing,
      int? countMembers,
      int? countActivities,
      String? timezone,
      String? firebaseToken,
      FileEntity? avatar});

  @override
  $FileEntityCopyWith<$Res>? get avatar;
}

/// @nodoc
class __$ProfileEntityCopyWithImpl<$Res>
    extends _$ProfileEntityCopyWithImpl<$Res>
    implements _$ProfileEntityCopyWith<$Res> {
  __$ProfileEntityCopyWithImpl(
      _ProfileEntity _value, $Res Function(_ProfileEntity) _then)
      : super(_value, (v) => _then(v as _ProfileEntity));

  @override
  _ProfileEntity get _value => super._value as _ProfileEntity;

  @override
  $Res call({
    Object? id = freezed,
    Object? email = freezed,
    Object? name = freezed,
    Object? countFollowers = freezed,
    Object? countFollowing = freezed,
    Object? countMembers = freezed,
    Object? countActivities = freezed,
    Object? timezone = freezed,
    Object? firebaseToken = freezed,
    Object? avatar = freezed,
  }) {
    return _then(_ProfileEntity(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      countFollowers: countFollowers == freezed
          ? _value.countFollowers
          : countFollowers // ignore: cast_nullable_to_non_nullable
              as int?,
      countFollowing: countFollowing == freezed
          ? _value.countFollowing
          : countFollowing // ignore: cast_nullable_to_non_nullable
              as int?,
      countMembers: countMembers == freezed
          ? _value.countMembers
          : countMembers // ignore: cast_nullable_to_non_nullable
              as int?,
      countActivities: countActivities == freezed
          ? _value.countActivities
          : countActivities // ignore: cast_nullable_to_non_nullable
              as int?,
      timezone: timezone == freezed
          ? _value.timezone
          : timezone // ignore: cast_nullable_to_non_nullable
              as String?,
      firebaseToken: firebaseToken == freezed
          ? _value.firebaseToken
          : firebaseToken // ignore: cast_nullable_to_non_nullable
              as String?,
      avatar: avatar == freezed
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as FileEntity?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ProfileEntity with DiagnosticableTreeMixin implements _ProfileEntity {
  _$_ProfileEntity(
      {@JsonKey(name: 'id') required this.id,
      required this.email,
      required this.name,
      this.countFollowers,
      this.countFollowing,
      this.countMembers,
      this.countActivities,
      this.timezone,
      this.firebaseToken,
      this.avatar});

  factory _$_ProfileEntity.fromJson(Map<String, dynamic> json) =>
      _$_$_ProfileEntityFromJson(json);

  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  final String email;
  @override
  final String name;
  @override
  final int? countFollowers;
  @override
  final int? countFollowing;
  @override
  final int? countMembers;
  @override
  final int? countActivities;
  @override
  final String? timezone;
  @override
  final String? firebaseToken;
  @override
  final FileEntity? avatar;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ProfileEntity(id: $id, email: $email, name: $name, countFollowers: $countFollowers, countFollowing: $countFollowing, countMembers: $countMembers, countActivities: $countActivities, timezone: $timezone, firebaseToken: $firebaseToken, avatar: $avatar)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ProfileEntity'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('email', email))
      ..add(DiagnosticsProperty('name', name))
      ..add(DiagnosticsProperty('countFollowers', countFollowers))
      ..add(DiagnosticsProperty('countFollowing', countFollowing))
      ..add(DiagnosticsProperty('countMembers', countMembers))
      ..add(DiagnosticsProperty('countActivities', countActivities))
      ..add(DiagnosticsProperty('timezone', timezone))
      ..add(DiagnosticsProperty('firebaseToken', firebaseToken))
      ..add(DiagnosticsProperty('avatar', avatar));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ProfileEntity &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.email, email) ||
                const DeepCollectionEquality().equals(other.email, email)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.countFollowers, countFollowers) ||
                const DeepCollectionEquality()
                    .equals(other.countFollowers, countFollowers)) &&
            (identical(other.countFollowing, countFollowing) ||
                const DeepCollectionEquality()
                    .equals(other.countFollowing, countFollowing)) &&
            (identical(other.countMembers, countMembers) ||
                const DeepCollectionEquality()
                    .equals(other.countMembers, countMembers)) &&
            (identical(other.countActivities, countActivities) ||
                const DeepCollectionEquality()
                    .equals(other.countActivities, countActivities)) &&
            (identical(other.timezone, timezone) ||
                const DeepCollectionEquality()
                    .equals(other.timezone, timezone)) &&
            (identical(other.firebaseToken, firebaseToken) ||
                const DeepCollectionEquality()
                    .equals(other.firebaseToken, firebaseToken)) &&
            (identical(other.avatar, avatar) ||
                const DeepCollectionEquality().equals(other.avatar, avatar)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(email) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(countFollowers) ^
      const DeepCollectionEquality().hash(countFollowing) ^
      const DeepCollectionEquality().hash(countMembers) ^
      const DeepCollectionEquality().hash(countActivities) ^
      const DeepCollectionEquality().hash(timezone) ^
      const DeepCollectionEquality().hash(firebaseToken) ^
      const DeepCollectionEquality().hash(avatar);

  @JsonKey(ignore: true)
  @override
  _$ProfileEntityCopyWith<_ProfileEntity> get copyWith =>
      __$ProfileEntityCopyWithImpl<_ProfileEntity>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ProfileEntityToJson(this);
  }
}

abstract class _ProfileEntity implements ProfileEntity {
  factory _ProfileEntity(
      {@JsonKey(name: 'id') required int id,
      required String email,
      required String name,
      int? countFollowers,
      int? countFollowing,
      int? countMembers,
      int? countActivities,
      String? timezone,
      String? firebaseToken,
      FileEntity? avatar}) = _$_ProfileEntity;

  factory _ProfileEntity.fromJson(Map<String, dynamic> json) =
      _$_ProfileEntity.fromJson;

  @override
  @JsonKey(name: 'id')
  int get id => throw _privateConstructorUsedError;
  @override
  String get email => throw _privateConstructorUsedError;
  @override
  String get name => throw _privateConstructorUsedError;
  @override
  int? get countFollowers => throw _privateConstructorUsedError;
  @override
  int? get countFollowing => throw _privateConstructorUsedError;
  @override
  int? get countMembers => throw _privateConstructorUsedError;
  @override
  int? get countActivities => throw _privateConstructorUsedError;
  @override
  String? get timezone => throw _privateConstructorUsedError;
  @override
  String? get firebaseToken => throw _privateConstructorUsedError;
  @override
  FileEntity? get avatar => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$ProfileEntityCopyWith<_ProfileEntity> get copyWith =>
      throw _privateConstructorUsedError;
}
