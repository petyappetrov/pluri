// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ProfileEntity _$_$_ProfileEntityFromJson(Map<String, dynamic> json) {
  return _$_ProfileEntity(
    id: json['id'] as int,
    email: json['email'] as String,
    name: json['name'] as String,
    countFollowers: json['countFollowers'] as int?,
    countFollowing: json['countFollowing'] as int?,
    countMembers: json['countMembers'] as int?,
    countActivities: json['countActivities'] as int?,
    timezone: json['timezone'] as String?,
    firebaseToken: json['firebaseToken'] as String?,
    avatar: json['avatar'] == null
        ? null
        : FileEntity.fromJson(json['avatar'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$_$_ProfileEntityToJson(_$_ProfileEntity instance) =>
    <String, dynamic>{
      'id': instance.id,
      'email': instance.email,
      'name': instance.name,
      'countFollowers': instance.countFollowers,
      'countFollowing': instance.countFollowing,
      'countMembers': instance.countMembers,
      'countActivities': instance.countActivities,
      'timezone': instance.timezone,
      'firebaseToken': instance.firebaseToken,
      'avatar': instance.avatar,
    };
