import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:pluri/authenticate/authtenticate.dart';
import 'package:pluri/profile/profile.dart';
import 'package:redux/redux.dart';
import 'package:pluri/root/root.dart';
import 'package:pluri/store/store.dart';

class ProfileSettingsScreen extends StatelessWidget {
  const ProfileSettingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: StoreConnector<AppState, _ViewModel>(
          converter: _ViewModel.fromStore,
          builder: (context, vm) {
            return SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TopBarUI(title: 'Настройки', back: true),
                  Container(
                    padding: EdgeInsets.fromLTRB(24, 0, 24, 24),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            vm.onLogout();
                          },
                          child: Text('Выйти'),
                        ),
                        SizedBox(height: 16),
                        ElevatedButton(
                          onPressed: () {
                            vm.onLogout();
                          },
                          child: Text('Удалить аккаунт'),
                        ),
                        SizedBox(height: 16),
                        ElevatedButton(
                          onPressed: () {
                            vm.onChangeThemeMode(ThemeMode.light);
                          },
                          child: Text('Светлая тема'),
                        ),
                        SizedBox(height: 16),
                        ElevatedButton(
                          onPressed: () {
                            vm.onChangeThemeMode(ThemeMode.dark);
                          },
                          // child: Text('Темная тема'),
                          child: Text('Темная тема'),
                        ),
                        SizedBox(height: 16),
                        ElevatedButton(
                          onPressed: () {
                            vm.onChangeThemeMode(ThemeMode.system);
                          },
                          child: Text('Системная тема'),
                        ),
                        SizedBox(height: 16),
                      ],
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(top: 24),
                    child: Text(
                      vm.profile?.id.toString() ?? '',
                      style: TextStyle(
                        color: Colors.white.withOpacity(0.5),
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(top: 16),
                    child: Text(
                      'version 0.0.1',
                      style: TextStyle(
                        color: Colors.white.withOpacity(0.5),
                      ),
                    ),
                  ),
                  SizedBox(height: 24),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}

class _ViewModel {
  final Function onLogout;
  final Function(ThemeMode themeMode) onChangeThemeMode;
  final ProfileEntity? profile;

  _ViewModel({
    required this.onLogout,
    required this.onChangeThemeMode,
    required this.profile,
  });

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
      onLogout: () {
        store.dispatch(AuthenticateLogoutAction());
      },
      onChangeThemeMode: (ThemeMode themeMode) {
        store.dispatch(RootChangeThemeModeAction(themeMode: themeMode));
      },
      profile: selectProfile(store.state),
    );
  }
}
