import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pluri/activites/activities.dart';
import 'package:pluri/profile/profile.dart';
import 'package:pluri/profile/screens/profile_settings_screen.dart';
import 'package:pluri/root/root.dart';
import 'package:pluri/store/store.dart';
import 'package:redux/redux.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      onInitialBuild: (viewModel) {
        if (viewModel.profile == null) {
          viewModel.loadProfile();
        }
      },
      converter: _ViewModel.fromStore,
      builder: (context, viewModel) {
        if (viewModel.profile == null) {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        }

        final isEmptyActivities = viewModel.activities == null || viewModel.activities?.length == 0;

        return CustomScrollView(
          slivers: [
            SliverAppBar(
              backgroundColor: Theme.of(context).scaffoldBackgroundColor,
              toolbarHeight: 100,
              floating: true,
              elevation: 0,
              primary: false,
              flexibleSpace: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  TopBarUI(
                    title: 'Мой профиль',
                    control: Container(
                      decoration: BoxDecoration(
                        color: Theme.of(context).colorScheme.onPrimary,
                        borderRadius: BorderRadius.all(
                          Radius.circular(12),
                        ),
                      ),
                      child: IconButton(
                        icon: Icon(FontAwesomeIcons.gear, size: 18),
                        color: Theme.of(context).colorScheme.primary,
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => ProfileSettingsScreen()),
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
            CupertinoSliverRefreshControl(
              onRefresh: viewModel.loadProfile,
            ),
            SliverPadding(
              padding: EdgeInsets.only(bottom: 16.0),
              sliver: SliverToBoxAdapter(child: ProfileCard(profile: viewModel.profile!)),
            ),
            SliverToBoxAdapter(child: ProfileAchievements()),
            if (!isEmptyActivities)
              SliverAppBar(
                elevation: 0,
                primary: true,
                flexibleSpace: Padding(
                  padding: EdgeInsets.fromLTRB(40, 24, 40, 0),
                  child: Text(
                    'Публикации',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                pinned: true,
                backgroundColor: Theme.of(context).scaffoldBackgroundColor,
              ),
            if (!isEmptyActivities)
              SliverList(
                delegate: SliverChildBuilderDelegate(
                  (context, index) => ActivityItem(
                    activity: viewModel.activities![index],
                  ),
                  childCount: viewModel.activities!.length,
                ),
              )
          ],
        );
      },
    );
  }
}

class _ViewModel {
  final Future<void> Function() loadProfile;
  final bool isLoading;
  final String? errorMessage;
  final ProfileEntity? profile;
  final List<ActivityEntity>? activities;

  _ViewModel({
    required this.profile,
    required this.activities,
    required this.loadProfile,
    required this.isLoading,
    required this.errorMessage,
  });

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
      profile: selectProfile(store.state),
      activities: selectProfileActivities(store.state),
      loadProfile: () async {
        final action = new ProfileLoadAction();
        store.dispatch(action);
        return action.completer!.future;
      },
      isLoading: selectProfileIsLoading(store.state),
      errorMessage: selectProfileErrorMessage(store.state),
    );
  }
}
