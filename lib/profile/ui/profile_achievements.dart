import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pluri/root/root.dart';

class ProfileAchievements extends StatelessWidget {
  const ProfileAchievements({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BoxUI(
      child: Padding(
        padding: EdgeInsets.fromLTRB(24, 24, 24, 24),
        child: Row(
          children: [
            Container(
              width: 60,
              height: 60,
              decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.primary,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Icon(
                FontAwesomeIcons.trophy,
                size: 22,
                color: Theme.of(context).colorScheme.onPrimary,
              ),
            ),
            SizedBox(width: 24),
            Text(
              'Награды',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Icon(
                  FontAwesomeIcons.chevronRight,
                  size: 16,
                  // color: Colors.black.withOpacity(0.5),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
