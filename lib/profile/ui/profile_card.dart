import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pluri/profile/profile.dart';
import 'package:pluri/root/root.dart';

class ProfileCard extends StatelessWidget {
  final ProfileEntity profile;

  const ProfileCard({
    Key? key,
    required this.profile,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BoxUI(
      child: Column(
        children: [
          SizedBox(height: 32),
          // Container(
          //   width: 100.0,
          //   height: 100.0,
          //   padding: EdgeInsets.all(4),
          //   child: ClipRRect(
          //     borderRadius: BorderRadius.circular(24),
          //     child: Image.network("http://localhost:3000/upload/" + profile.avatar!.id.toString()),
          //   ),
          // ),
          AvatarUI(
            width: 120,
            height: 120,
            isCircle: true,
            isBordered: true,
            profile: profile,
          ),
          SizedBox(height: 24),
          Text(
            profile.name,
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 24),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    // profile.countFollowers.toString(),
                    '5',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 2),
                  Text(
                    'Публикаций',
                    style: TextStyle(
                      fontSize: 13,
                      color: Theme.of(context).textTheme.bodyText1?.color?.withOpacity(0.6),
                    ),
                  ),
                ],
              ),
              SizedBox(width: 16),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    // profile.countFollowing.toString(),
                    '12',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 2),
                  Text(
                    'Подписок',
                    style: TextStyle(
                      fontSize: 13,
                      color: Theme.of(context).textTheme.bodyText1?.color?.withOpacity(0.6),
                    ),
                  ),
                ],
              ),
              SizedBox(width: 16),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    // profile.countFollowers.toString(),
                    '11',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 2),
                  Text(
                    'Подписчиков',
                    style: TextStyle(
                      fontSize: 13,
                      color: Theme.of(context).textTheme.bodyText1?.color?.withOpacity(0.6),
                    ),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(height: 24),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                style: ButtonStyle(
                  padding: MaterialStateProperty.resolveWith<EdgeInsetsGeometry>(
                    (Set<MaterialState> states) {
                      return EdgeInsets.fromLTRB(24, 15, 24, 15);
                    },
                  ),
                ),
                onPressed: () {},
                child: Text(
                  'Подписаться',
                  style: TextStyle(fontSize: 16),
                ),
              ),
              SizedBox(
                width: 16,
              ),
              Container(
                padding: EdgeInsets.all(0),
                decoration: BoxDecoration(
                  color: Colors.blueGrey.shade50,
                  borderRadius: BorderRadius.all(
                    Radius.circular(12),
                  ),
                ),
                child: IconButton(
                  icon: Icon(FontAwesomeIcons.solidEnvelope, size: 18),
                  color: Colors.black,
                  onPressed: () {},
                ),
              ),
            ],
          ),
          SizedBox(height: 24),
        ],
      ),
    );
  }
}
