import 'dart:async';

import 'package:pluri/activites/activities.dart';
import 'package:pluri/profile/profile.dart';
import 'package:pluri/profile/entities/entities.dart';

class ProfileLoadAction {
  final Completer<dynamic>? completer;

  ProfileLoadAction({Completer? completer}) : this.completer = completer ?? Completer<dynamic>();
}

class ProfileLoadSuccessAction {
  final ProfileEntity profile;
  ProfileLoadSuccessAction({required this.profile});
}

class ProfileLoadFailureAction {
  final String errorMessage;
  ProfileLoadFailureAction({required this.errorMessage});
}

class ProfileActivitiesLoadAction {
  ProfileActivitiesLoadAction();
}

class ProfileActivitiesLoadSuccessAction {
  final List<ActivityEntity> activities;
  ProfileActivitiesLoadSuccessAction({required this.activities});
}

class ProfileActivitiesLoadFailureAction {
  final String errorMessage;
  ProfileActivitiesLoadFailureAction({required this.errorMessage});
}

class ProfileRemoveFirebaseTokenAction {}

class ProfileRemoveFirebaseTokenSuccessAction {}

class ProfileRemoveFirebaseTokenFailureAction {
  final String errorMessage;
  ProfileRemoveFirebaseTokenFailureAction({required this.errorMessage});
}

class ProfileChangeFirebaseTokenAction {
  final String firebaseToken;
  ProfileChangeFirebaseTokenAction({required this.firebaseToken});
}

class ProfileChangeFirebaseTokenSuccessAction {
  final String firebaseToken;
  ProfileChangeFirebaseTokenSuccessAction({required this.firebaseToken});
}

class ProfileChangeFirebaseTokenFailureAction {
  final String errorMessage;
  ProfileChangeFirebaseTokenFailureAction({required this.errorMessage});
}

class ProfileChangeTimezoneAction {
  final String timezone;
  ProfileChangeTimezoneAction({required this.timezone});
}

class ProfileChangeTimezoneSuccessAction {
  final ProfileEntity profile;
  ProfileChangeTimezoneSuccessAction({required this.profile});
}

class ProfileChangeTimezoneFailureAction {
  final String errorMessage;
  ProfileChangeTimezoneFailureAction({required this.errorMessage});
}

class ProfileEnableNotificationsAction {}

class ProfileEnableNotificationsSuccessAction {}

class ProfileEnableNotificationsFailureAction {
  final String errorMessage;
  ProfileEnableNotificationsFailureAction({required this.errorMessage});
}

class ProfileDisableNotificationsAction {}

class ProfileDisableNotificationsSuccessAction {}

class ProfileDisableNotificationsFailureAction {
  final String errorMessage;
  ProfileDisableNotificationsFailureAction({required this.errorMessage});
}
