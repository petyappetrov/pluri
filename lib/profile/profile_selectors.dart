import 'package:pluri/activites/activities.dart';
import 'package:pluri/profile/profile.dart';
import 'package:pluri/store/store.dart';
import 'package:reselect/reselect.dart';

final selectProfileState = (AppState store) => store.profile;

final selectProfile = createSelector1<AppState, ProfileState, ProfileEntity?>(
  selectProfileState,
  (state) => state.profile,
);

final selectProfileActivities = createSelector1<AppState, ProfileState, List<ActivityEntity>?>(
  selectProfileState,
  (state) => state.activities,
);

final selectProfileIsLoading = createSelector1<AppState, ProfileState, bool>(
  selectProfileState,
  (state) => state.isLoading,
);

final selectProfileErrorMessage = createSelector1<AppState, ProfileState, String?>(
  selectProfileState,
  (state) => state.errorMessage,
);
