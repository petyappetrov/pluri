import 'package:pluri/activites/activities.dart';
import 'package:pluri/http_client.dart';
import 'package:pluri/profile/profile.dart';
import 'package:dio/dio.dart';

class ProfileService {
  static Future<ProfileEntity> getProfile() async {
    final Response response = await dio.get('/profile');
    return ProfileEntity.fromJson(response.data);
  }

  static Future<List<ActivityEntity>> getProfileActivities() async {
    final response = await dio.get('/profile/activities');
    final List<ActivityEntity> activities = response.data!
        .map<ActivityEntity>(
          (json) => ActivityEntity.fromJson(json),
        )
        .toList();

    return activities;
  }
}
