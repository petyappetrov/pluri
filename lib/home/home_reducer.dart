import 'package:pluri/home/home.dart';

HomeState homeReducer(HomeState state, action) {
  if (action is HomeChangeDestinationIndex) {
    return state.copyWith(
      destinationIndex: action.index,
    );
  }
  return state;
}
