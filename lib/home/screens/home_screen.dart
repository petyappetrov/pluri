// import 'package:flushbar/flushbar.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pluri/activites/activities.dart';
import 'package:pluri/challenges/challenges.dart';
import 'package:pluri/home/home.dart';
import 'package:pluri/notifications/notifications.dart';
import 'package:pluri/profile/profile.dart';
import 'package:pluri/search/search.dart';
import 'package:pluri/store/store.dart';
import 'package:redux/redux.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({Key? key}) : super(key: key);

  List<HomeDestination> get destinations {
    return [
      HomeDestination(
        title: 'Лента',
        icon: FontAwesomeIcons.fire,
        body: ActivitiesScreen(key: PageStorageKey('activities-screen')),
      ),
      HomeDestination(
        title: 'Челленджи',
        icon: FontAwesomeIcons.stream,
        body: ChallengesScreen(key: PageStorageKey('challenges-screen')),
      ),
      HomeDestination(
        title: 'Новая публикация',
        icon: FontAwesomeIcons.plusCircle,
        body: ChallengesScreen(key: PageStorageKey('challenges-screen')),
      ),
      HomeDestination(
        title: 'Активность',
        icon: FontAwesomeIcons.search,
        body: NotificationsScreen(key: PageStorageKey('notifications-screen')),
      ),
      HomeDestination(
        title: 'Профиль',
        icon: FontAwesomeIcons.solidUser,
        body: ProfileScreen(key: PageStorageKey('profile-screen')),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: _ViewModel.fromStore,
      distinct: true,
      // onWillChange: (prevState, nextState) {
      //   if (nextState.message != prevState.message) {
      //     Flushbar(
      //       forwardAnimationCurve: Curves.easeOutBack,
      //       reverseAnimationCurve: Curves.fastOutSlowIn,
      //       animationDuration: Duration(milliseconds: 550),
      //       titleText: Text(
      //         nextState.message.notification.title,
      //         style: TextStyle(
      //           color: Colors.white,
      //           fontWeight: FontWeight.bold,
      //           fontSize: 16,
      //         ),
      //       ),
      //       messageText: Text(
      //         nextState.message.notification.body,
      //         style: TextStyle(color: Colors.white),
      //       ),
      //       duration: Duration(seconds: 4),
      //       margin: EdgeInsets.fromLTRB(24, 8, 24, 0),
      //       borderRadius: 16,
      //       flushbarPosition: FlushbarPosition.TOP,
      //       backgroundColor: Theme.of(context).accentColor,
      //     )..show(context);
      //   }
      // },
      builder: (context, vm) {
        return Scaffold(
          appBar: AppBar(
             backgroundColor: Colors.transparent, 
             elevation: 0,
             toolbarHeight: 0,
            systemOverlayStyle: SystemUiOverlayStyle.dark, // status bar brightness
          ),
          bottomNavigationBar: NavigationBarUI(
            destinations: destinations,
            currentIndex: vm.destinationIndex,
            changeDestinationIndex: (int index) {
              vm.changeDestinationIndex(index);
            },
          ),
          body: SafeArea(
            child: destinations[vm.destinationIndex].body,
          ),
        );
      },
    );
  }
}

class _ViewModel {
  final int destinationIndex;
  final Function(int) changeDestinationIndex;
  // final FirebaseMessageEntity message;

  _ViewModel({
    required this.destinationIndex,
    required this.changeDestinationIndex,
    // required this.message,
  });

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
      destinationIndex: store.state.home.destinationIndex,
      changeDestinationIndex: (index) {
        store.dispatch(HomeChangeDestinationIndex(index: index));
      },
      // message: firebaseMessagesFirebaseMessageSelector(store.state),
    );
  }
}
