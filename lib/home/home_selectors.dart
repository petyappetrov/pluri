import 'package:pluri/home/home.dart';
import 'package:pluri/store/store.dart';
import 'package:reselect/reselect.dart';

final _homeStateSelector = (AppState store) => store.home;

final homeDestinationIndexSelector = createSelector1<
  AppState,
  HomeState,
  int
>(_homeStateSelector, (state) => state.destinationIndex);
