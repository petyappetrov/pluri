import 'package:flutter/material.dart';
import 'package:pluri/home/home.dart';
import 'package:flutter/services.dart';

class NavigationBarUI extends StatelessWidget {
  final List<HomeDestination> destinations;
  final int currentIndex;
  final Function changeDestinationIndex;

  const NavigationBarUI({
    Key? key,
    required this.destinations,
    required this.currentIndex,
    required this.changeDestinationIndex,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120.00,
      padding: MediaQuery.of(context).padding,
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.onPrimary,
        // boxShadow: [
        //   BoxShadow(
        //     color: Colors.black.withOpacity(0.08),
        //     blurRadius: 24,
        //   ),
        // ],
        border: Border.all(
          color: Theme.of(context).colorScheme.primary.withOpacity(0.14),
          width: 0.5,
        ),
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(24.0),
          topLeft: Radius.circular(24.0),
        ),
        // borderRadius: new BorderRadius.only(
        //   topLeft: const Radius.circular(24),
        //   topRight: const Radius.circular(24),
        // )
        // border: Border.all(width: 1, color: Colors.black.withOpacity(0.1)),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: destinations.map((destination) {
            final int index = destinations.indexOf(destination);
            final bool isSelected = currentIndex == index;
            return GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                if (isSelected) {
                  return;
                }
                HapticFeedback.selectionClick();
                changeDestinationIndex(index);
              },
              child: Container(
                width: (MediaQuery.of(context).size.width - 42) / destinations.length,
                child: Center(
                  child: Icon(
                    destination.icon,
                    color: isSelected ? Theme.of(context).colorScheme.primary : Colors.blueGrey.shade300,
                  ),
                ),
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}
