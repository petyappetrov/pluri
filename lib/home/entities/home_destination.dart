import 'package:flutter/material.dart';

class HomeDestination {
  final String title;
  final IconData icon;
  final Widget body;

  HomeDestination({
    required this.title,
    required this.icon,
    required this.body,
  });
}
