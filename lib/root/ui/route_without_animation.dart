import 'package:flutter/material.dart';

class RouteWithoutAnimation<T> extends MaterialPageRoute<T> {
  RouteWithoutAnimation({
    required WidgetBuilder builder,
    required RouteSettings settings,
  }) : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child
  ) {
    return child;
  }
}
