import 'package:flutter/widgets.dart';

class CustomIcons {
  CustomIcons._();

  static const _kFontFam = 'Custom';
  static const _kFontPkg = null;

  static const IconData profile = IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData words = IconData(0xe802, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData progress = IconData(0xe803, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData notifications = IconData(0xe804, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData filter = IconData(0xe805, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
