import 'package:flutter/material.dart';

class BoxUI extends StatelessWidget {
  final Widget child;
  final Color? color;

  const BoxUI({Key? key, required this.child, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(24, 0, 24, 0),
      // padding: EdgeInsets.fromLTRB(24, 24, 24, 24),
      width: double.infinity,
      clipBehavior: Clip.antiAlias,
      decoration: BoxDecoration(
        color: color == null ? Theme.of(context).colorScheme.onPrimary : color,
        borderRadius: BorderRadius.all(
          Radius.circular(24),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.blueAccent.withOpacity(0.08),
            offset: Offset(0, 24),
            spreadRadius: -24,
            blurRadius: 16,
          ),
        ],
      ),
      child: child,
    );
  }
}
