import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class RadioButton extends StatelessWidget {
  final String value;
  final String groupValue;
  final String label;
  final Function onPressed;

  RadioButton({
    Key? key,
    required this.value,
    required this.groupValue,
    required this.label,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        HapticFeedback.selectionClick();
        onPressed();
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 8),
        child: Row(
          children: [
            SizedBox(
              width: 18,
              height: 18,
              child: Radio(
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                value: value,
                groupValue: groupValue,
                onChanged: (value) {},
                activeColor: Theme.of(context).accentColor,
              ),
            ),
            SizedBox(width: 12),
            Text(
              label,
              style: TextStyle(
                color: value == groupValue
                  ? Theme.of(context).accentColor
                  : Colors.white
              ),
            )
          ],
        ),
      ),
    );
  }
}
