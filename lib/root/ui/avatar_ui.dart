import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pluri/profile/profile.dart';

class AvatarUI extends StatelessWidget {
  final ProfileEntity? profile;
  final double width;
  final double height;
  final bool isCircle;
  final bool isBordered;
  final String? url;

  const AvatarUI({
    Key? key,
    this.width = 60,
    this.height = 60,
    this.url,
    this.profile,
    this.isCircle = false,
    this.isBordered = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      clipBehavior: Clip.antiAlias,
      padding: isBordered ? EdgeInsets.all(4) : null,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(isCircle ? width / 2 : 20),
      ),
      child: url != null
          ? ClipRRect(
              borderRadius: BorderRadius.circular(isCircle ? width / 2 : 20),
              child: Image.file(
                File(url!),
                width: width,
                height: height,
                fit: BoxFit.cover,
              ),
            )
          : profile != null
              ? Container(
                  decoration: BoxDecoration(
                    color: Colors.blueGrey,
                    borderRadius: BorderRadius.circular(isCircle ? width / 2 : 20),
                  ),
                  child: profile!.avatar == null
                      ? Center(
                          child: Text(
                            profile!.name.substring(0, 2).toUpperCase(),
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 22,
                              fontWeight: FontWeight.w900,
                            ),
                          ),
                        )
                      : Container(
                          child: Image.network("http://localhost:3000/upload/" + profile!.avatar!.id.toString()),
                        ),
                )
              : Icon(
                  FontAwesomeIcons.camera,
                  size: 48,
                  color: Theme.of(context).colorScheme.secondary,
                ),
    );
  }
}
