import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TopBarUI extends StatelessWidget {
  final String title;
  final bool? back;
  final Widget? control;

  TopBarUI({
    Key? key,
    this.control,
    this.back,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (back != null)
          Container(
            margin: EdgeInsets.fromLTRB(24, 24, 0, 60),
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.onPrimary,
              borderRadius: BorderRadius.all(
                Radius.circular(12),
              ),
            ),
            child: IconButton(
              icon: Icon(FontAwesomeIcons.chevronLeft, size: 16),
              color: Theme.of(context).colorScheme.primary,
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        Container(
          padding: EdgeInsets.fromLTRB(24, 24, 24, 0),
          height: 100,
          alignment: Alignment.topCenter,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                title,
                style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 28,
                ),
              ),
              if (control != null) control!,
            ],
          ),
        ),
      ],
    );
  }
}
