import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

enum ButtonVariant {
  orange,
  dark,
  transparent,
  primary,
  green,
  red,
}

enum ButtonSize { small, medium, large }

class ButtonUI extends StatelessWidget {
  final String label;
  final Function onPressed;
  final ButtonVariant variant;
  final ButtonSize size;
  final bool isLoading;

  ButtonUI({
    Key? key,
    required this.label,
    required this.onPressed,
    this.variant = ButtonVariant.orange,
    this.size = ButtonSize.medium,
    this.isLoading = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {
        if (isLoading) {
          return;
        }
        HapticFeedback.selectionClick();
        onPressed();
      },
      style: TextButton.styleFrom(
        padding: EdgeInsets.all(14.0),
        backgroundColor: _getVariantColor(context),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(16)),
        ),
      ),
      child: Text(
        label,
        style: TextStyle(
          fontSize: _getFontSizeButton(),
          fontWeight: FontWeight.bold,
          color: variant == ButtonVariant.transparent ? Colors.black : Colors.white,
        ),
      ),
    );
  }

  Color _getVariantColor(BuildContext context) {
    switch (variant) {
      case ButtonVariant.dark:
        return Colors.black;
      case ButtonVariant.primary:
        return Theme.of(context).scaffoldBackgroundColor;
      case ButtonVariant.transparent:
        return Colors.transparent;
      case ButtonVariant.green:
        return Colors.greenAccent;
      case ButtonVariant.red:
        return Color.fromRGBO(241, 104, 149, 1);
      case ButtonVariant.orange:
      default:
        return Theme.of(context).accentColor;
    }
  }

  double _getFontSizeButton() {
    switch (size) {
      case ButtonSize.small:
        return 12;
      default:
        return 16;
    }
  }
}
