import 'package:flutter/material.dart';

class RootStartupApplicationAction {}

class RootStartupApplicationSuccessAction {}

class RootStartupApplicationFailureAction {
  final String errorMessage;
  RootStartupApplicationFailureAction({
    required this.errorMessage,
  });
}

class RootChangeThemeModeAction {
  final ThemeMode themeMode;
  RootChangeThemeModeAction({
    required this.themeMode,
  });
}
