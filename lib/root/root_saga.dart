import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:pluri/authenticate/authenticate_actions.dart';
import 'package:pluri/root/root.dart';
import 'package:redux_saga/redux_saga.dart';
import 'package:shared_preferences/shared_preferences.dart';

takeThemeModeFromStorage() sync* {
  var prefs = Result<SharedPreferences>();

  yield Call(SharedPreferences.getInstance, result: prefs);

  final themeMode = EnumToString.fromString(
    ThemeMode.values,
    prefs.value?.getString('theme_mode') ?? 'system',
  );

  yield Return(themeMode);
}

saveThemeModeToStorage({
  required RootChangeThemeModeAction action,
}) sync* {
  final prefs = Result<SharedPreferences>();

  yield Call(SharedPreferences.getInstance, result: prefs);

  prefs.value!.setString('theme_mode', EnumToString.convertToString(action.themeMode));
}

rootInitialize({required RootStartupApplicationAction action}) sync* {
  yield Try(() sync* {
      final themeMode = Result<ThemeMode>();

      yield Call(takeThemeModeFromStorage, result: themeMode);

      yield Put(RootChangeThemeModeAction(themeMode: themeMode.value!));

      yield Put(AuthenticateInitialzeAction());

      yield Take(pattern: AuthenticateInitialzeSuccessAction);

      yield Put(RootStartupApplicationSuccessAction());
    }, Catch: (error, s) sync* {
      yield Put(
        RootStartupApplicationFailureAction(
          errorMessage: 'Ошибка инициализации приложения',
        ),
      );
    });
}

watchRootInitialize() sync* {
  yield TakeLatest(rootInitialize, pattern: RootStartupApplicationAction);
}

watchRootChangeThemeMode() sync* {
  yield TakeLatest(saveThemeModeToStorage, pattern: RootChangeThemeModeAction);
}