import 'package:flutter/material.dart';
import 'package:pluri/root/root.dart';
import 'package:pluri/store/store.dart';
import 'package:reselect/reselect.dart';

final selectRootState = (AppState store) => store.root;

final selectRootErrorMessage = createSelector1<AppState, RootState, String?>(
  selectRootState,
  (state) => state.errorMessage,
);

final selectRootThemeMode = createSelector1<AppState, RootState, ThemeMode>(
  selectRootState,
  (state) => state.themeMode,
);
