import 'package:dio/dio.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pluri/http_client.dart';

class RootService {
  static Future<String> uploadPhoto(XFile photo) async {
    var formData = FormData.fromMap({
      'file': await MultipartFile.fromFile(
        photo.path,
        filename: photo.name,
      )
    });
    final response = await dio.post('/upload', data: formData);
    return response.data['url'];
  }
}
