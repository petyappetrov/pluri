import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';
import 'package:pluri/authenticate/screens/authenticate_register_screen.dart';
import 'package:pluri/home/home.dart';
import 'package:pluri/onboarding/screens/screens.dart';
import 'package:pluri/profile/profile.dart';
import 'package:pluri/root/root.dart';
import 'package:pluri/store/store.dart';
import 'package:pluri/theme/theme.dart';
import 'package:redux/redux.dart';

class RootScreen extends StatelessWidget {
  RootScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: _ViewModel.fromStore,
      distinct: true,
      onInit: (store) {
        store.dispatch(RootStartupApplicationAction());
      },
      builder: (context, viewModel) {
        return MaterialApp(
          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: [
            Locale('ru'),
            Locale('en'),
          ],
          debugShowCheckedModeBanner: false,
          title: 'Pluri',
          theme: lightTheme,
          darkTheme: darkTheme,
          themeMode: viewModel.themeMode,
          navigatorKey: NavigatorHolder.navigatorKey,
          initialRoute: '/',
          onGenerateRoute: (RouteSettings settings) {
            switch (settings.name) {
              case '/onboarding':
                return RouteWithoutAnimation(
                  builder: (context) {
                    return OnboardingScreen();
                  },
                  settings: settings,
                );
              // case '/register':
              //   return MaterialPageRoute(
              //     settings: settings,
              //     builder: (context) => AuthenticateRegisterScreen(),
              //   );
              // case '/register':
              //   return MaterialPageRoute(
              //     settings: settings,
              //     builder: (context) => AuthenticateRegisterScreen(),
              //   );
              case '/home':
              default:
                return RouteWithoutAnimation(
                  settings: settings,
                  builder: (context) => HomeScreen(),
                );
            }
          },
        );
      },
    );
  }
}

class _ViewModel {
  final ThemeMode themeMode;
  final Function(ThemeMode) changeThemeMode;

  _ViewModel({
    required this.themeMode,
    required this.changeThemeMode,
  });

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
      themeMode: selectRootThemeMode(store.state),
      changeThemeMode: (themeMode) {
        store.dispatch(RootChangeThemeModeAction(themeMode: themeMode));
      },
    );
  }
}
