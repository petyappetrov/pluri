import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'file_entity.freezed.dart';
part 'file_entity.g.dart';

@freezed
abstract class FileEntity with _$FileEntity {
  factory FileEntity({
    @JsonKey(name: 'id') required int id,
    required String createdDate,
    required String url,
    required String key,
    String? photo,
  }) = _FileEntity;

  factory FileEntity.fromJson(Map<String, dynamic> json) =>
    _$FileEntityFromJson(json);
}
