import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'root_state.freezed.dart';
part 'root_state.g.dart';

@freezed
abstract class RootState with _$RootState {
  factory RootState({
    @Default(false) bool isStarting,
    @Default(ThemeMode.system) ThemeMode themeMode,
    String? errorMessage,
  }) = _RootState;

  factory RootState.fromJson(Map<String, dynamic> json) => _$RootStateFromJson(json);

  static getPersistedState() async {
    final instance = await SharedPreferences.getInstance();
    final themeMode = EnumToString.fromString(
      ThemeMode.values,
      instance.getString('theme_mode') ?? '',
    );

    if (themeMode != null) {
      return RootState(themeMode: themeMode);
    }

    return RootState();
  }
}
