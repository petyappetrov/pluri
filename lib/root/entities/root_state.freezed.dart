// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'root_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

RootState _$RootStateFromJson(Map<String, dynamic> json) {
  return _RootState.fromJson(json);
}

/// @nodoc
class _$RootStateTearOff {
  const _$RootStateTearOff();

  _RootState call(
      {bool isStarting = false,
      ThemeMode themeMode = ThemeMode.system,
      String? errorMessage}) {
    return _RootState(
      isStarting: isStarting,
      themeMode: themeMode,
      errorMessage: errorMessage,
    );
  }

  RootState fromJson(Map<String, Object> json) {
    return RootState.fromJson(json);
  }
}

/// @nodoc
const $RootState = _$RootStateTearOff();

/// @nodoc
mixin _$RootState {
  bool get isStarting => throw _privateConstructorUsedError;
  ThemeMode get themeMode => throw _privateConstructorUsedError;
  String? get errorMessage => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RootStateCopyWith<RootState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RootStateCopyWith<$Res> {
  factory $RootStateCopyWith(RootState value, $Res Function(RootState) then) =
      _$RootStateCopyWithImpl<$Res>;
  $Res call({bool isStarting, ThemeMode themeMode, String? errorMessage});
}

/// @nodoc
class _$RootStateCopyWithImpl<$Res> implements $RootStateCopyWith<$Res> {
  _$RootStateCopyWithImpl(this._value, this._then);

  final RootState _value;
  // ignore: unused_field
  final $Res Function(RootState) _then;

  @override
  $Res call({
    Object? isStarting = freezed,
    Object? themeMode = freezed,
    Object? errorMessage = freezed,
  }) {
    return _then(_value.copyWith(
      isStarting: isStarting == freezed
          ? _value.isStarting
          : isStarting // ignore: cast_nullable_to_non_nullable
              as bool,
      themeMode: themeMode == freezed
          ? _value.themeMode
          : themeMode // ignore: cast_nullable_to_non_nullable
              as ThemeMode,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$RootStateCopyWith<$Res> implements $RootStateCopyWith<$Res> {
  factory _$RootStateCopyWith(
          _RootState value, $Res Function(_RootState) then) =
      __$RootStateCopyWithImpl<$Res>;
  @override
  $Res call({bool isStarting, ThemeMode themeMode, String? errorMessage});
}

/// @nodoc
class __$RootStateCopyWithImpl<$Res> extends _$RootStateCopyWithImpl<$Res>
    implements _$RootStateCopyWith<$Res> {
  __$RootStateCopyWithImpl(_RootState _value, $Res Function(_RootState) _then)
      : super(_value, (v) => _then(v as _RootState));

  @override
  _RootState get _value => super._value as _RootState;

  @override
  $Res call({
    Object? isStarting = freezed,
    Object? themeMode = freezed,
    Object? errorMessage = freezed,
  }) {
    return _then(_RootState(
      isStarting: isStarting == freezed
          ? _value.isStarting
          : isStarting // ignore: cast_nullable_to_non_nullable
              as bool,
      themeMode: themeMode == freezed
          ? _value.themeMode
          : themeMode // ignore: cast_nullable_to_non_nullable
              as ThemeMode,
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_RootState implements _RootState {
  _$_RootState(
      {this.isStarting = false,
      this.themeMode = ThemeMode.system,
      this.errorMessage});

  factory _$_RootState.fromJson(Map<String, dynamic> json) =>
      _$_$_RootStateFromJson(json);

  @JsonKey(defaultValue: false)
  @override
  final bool isStarting;
  @JsonKey(defaultValue: ThemeMode.system)
  @override
  final ThemeMode themeMode;
  @override
  final String? errorMessage;

  @override
  String toString() {
    return 'RootState(isStarting: $isStarting, themeMode: $themeMode, errorMessage: $errorMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _RootState &&
            (identical(other.isStarting, isStarting) ||
                const DeepCollectionEquality()
                    .equals(other.isStarting, isStarting)) &&
            (identical(other.themeMode, themeMode) ||
                const DeepCollectionEquality()
                    .equals(other.themeMode, themeMode)) &&
            (identical(other.errorMessage, errorMessage) ||
                const DeepCollectionEquality()
                    .equals(other.errorMessage, errorMessage)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(isStarting) ^
      const DeepCollectionEquality().hash(themeMode) ^
      const DeepCollectionEquality().hash(errorMessage);

  @JsonKey(ignore: true)
  @override
  _$RootStateCopyWith<_RootState> get copyWith =>
      __$RootStateCopyWithImpl<_RootState>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_RootStateToJson(this);
  }
}

abstract class _RootState implements RootState {
  factory _RootState(
      {bool isStarting,
      ThemeMode themeMode,
      String? errorMessage}) = _$_RootState;

  factory _RootState.fromJson(Map<String, dynamic> json) =
      _$_RootState.fromJson;

  @override
  bool get isStarting => throw _privateConstructorUsedError;
  @override
  ThemeMode get themeMode => throw _privateConstructorUsedError;
  @override
  String? get errorMessage => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$RootStateCopyWith<_RootState> get copyWith =>
      throw _privateConstructorUsedError;
}
