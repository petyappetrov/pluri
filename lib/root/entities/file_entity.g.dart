// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'file_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_FileEntity _$_$_FileEntityFromJson(Map<String, dynamic> json) {
  return _$_FileEntity(
    id: json['id'] as int,
    createdDate: json['createdDate'] as String,
    url: json['url'] as String,
    key: json['key'] as String,
    photo: json['photo'] as String?,
  );
}

Map<String, dynamic> _$_$_FileEntityToJson(_$_FileEntity instance) =>
    <String, dynamic>{
      'id': instance.id,
      'createdDate': instance.createdDate,
      'url': instance.url,
      'key': instance.key,
      'photo': instance.photo,
    };
