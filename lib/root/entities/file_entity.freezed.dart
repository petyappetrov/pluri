// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'file_entity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

FileEntity _$FileEntityFromJson(Map<String, dynamic> json) {
  return _FileEntity.fromJson(json);
}

/// @nodoc
class _$FileEntityTearOff {
  const _$FileEntityTearOff();

  _FileEntity call(
      {@JsonKey(name: 'id') required int id,
      required String createdDate,
      required String url,
      required String key,
      String? photo}) {
    return _FileEntity(
      id: id,
      createdDate: createdDate,
      url: url,
      key: key,
      photo: photo,
    );
  }

  FileEntity fromJson(Map<String, Object> json) {
    return FileEntity.fromJson(json);
  }
}

/// @nodoc
const $FileEntity = _$FileEntityTearOff();

/// @nodoc
mixin _$FileEntity {
  @JsonKey(name: 'id')
  int get id => throw _privateConstructorUsedError;
  String get createdDate => throw _privateConstructorUsedError;
  String get url => throw _privateConstructorUsedError;
  String get key => throw _privateConstructorUsedError;
  String? get photo => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FileEntityCopyWith<FileEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FileEntityCopyWith<$Res> {
  factory $FileEntityCopyWith(
          FileEntity value, $Res Function(FileEntity) then) =
      _$FileEntityCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id') int id,
      String createdDate,
      String url,
      String key,
      String? photo});
}

/// @nodoc
class _$FileEntityCopyWithImpl<$Res> implements $FileEntityCopyWith<$Res> {
  _$FileEntityCopyWithImpl(this._value, this._then);

  final FileEntity _value;
  // ignore: unused_field
  final $Res Function(FileEntity) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? createdDate = freezed,
    Object? url = freezed,
    Object? key = freezed,
    Object? photo = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      createdDate: createdDate == freezed
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as String,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
      key: key == freezed
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as String,
      photo: photo == freezed
          ? _value.photo
          : photo // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$FileEntityCopyWith<$Res> implements $FileEntityCopyWith<$Res> {
  factory _$FileEntityCopyWith(
          _FileEntity value, $Res Function(_FileEntity) then) =
      __$FileEntityCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id') int id,
      String createdDate,
      String url,
      String key,
      String? photo});
}

/// @nodoc
class __$FileEntityCopyWithImpl<$Res> extends _$FileEntityCopyWithImpl<$Res>
    implements _$FileEntityCopyWith<$Res> {
  __$FileEntityCopyWithImpl(
      _FileEntity _value, $Res Function(_FileEntity) _then)
      : super(_value, (v) => _then(v as _FileEntity));

  @override
  _FileEntity get _value => super._value as _FileEntity;

  @override
  $Res call({
    Object? id = freezed,
    Object? createdDate = freezed,
    Object? url = freezed,
    Object? key = freezed,
    Object? photo = freezed,
  }) {
    return _then(_FileEntity(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      createdDate: createdDate == freezed
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as String,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
      key: key == freezed
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as String,
      photo: photo == freezed
          ? _value.photo
          : photo // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_FileEntity with DiagnosticableTreeMixin implements _FileEntity {
  _$_FileEntity(
      {@JsonKey(name: 'id') required this.id,
      required this.createdDate,
      required this.url,
      required this.key,
      this.photo});

  factory _$_FileEntity.fromJson(Map<String, dynamic> json) =>
      _$_$_FileEntityFromJson(json);

  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  final String createdDate;
  @override
  final String url;
  @override
  final String key;
  @override
  final String? photo;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FileEntity(id: $id, createdDate: $createdDate, url: $url, key: $key, photo: $photo)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'FileEntity'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('createdDate', createdDate))
      ..add(DiagnosticsProperty('url', url))
      ..add(DiagnosticsProperty('key', key))
      ..add(DiagnosticsProperty('photo', photo));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _FileEntity &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.createdDate, createdDate) ||
                const DeepCollectionEquality()
                    .equals(other.createdDate, createdDate)) &&
            (identical(other.url, url) ||
                const DeepCollectionEquality().equals(other.url, url)) &&
            (identical(other.key, key) ||
                const DeepCollectionEquality().equals(other.key, key)) &&
            (identical(other.photo, photo) ||
                const DeepCollectionEquality().equals(other.photo, photo)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(createdDate) ^
      const DeepCollectionEquality().hash(url) ^
      const DeepCollectionEquality().hash(key) ^
      const DeepCollectionEquality().hash(photo);

  @JsonKey(ignore: true)
  @override
  _$FileEntityCopyWith<_FileEntity> get copyWith =>
      __$FileEntityCopyWithImpl<_FileEntity>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_FileEntityToJson(this);
  }
}

abstract class _FileEntity implements FileEntity {
  factory _FileEntity(
      {@JsonKey(name: 'id') required int id,
      required String createdDate,
      required String url,
      required String key,
      String? photo}) = _$_FileEntity;

  factory _FileEntity.fromJson(Map<String, dynamic> json) =
      _$_FileEntity.fromJson;

  @override
  @JsonKey(name: 'id')
  int get id => throw _privateConstructorUsedError;
  @override
  String get createdDate => throw _privateConstructorUsedError;
  @override
  String get url => throw _privateConstructorUsedError;
  @override
  String get key => throw _privateConstructorUsedError;
  @override
  String? get photo => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$FileEntityCopyWith<_FileEntity> get copyWith =>
      throw _privateConstructorUsedError;
}
