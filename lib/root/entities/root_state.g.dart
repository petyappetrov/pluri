// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'root_state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_RootState _$_$_RootStateFromJson(Map<String, dynamic> json) {
  return _$_RootState(
    isStarting: json['isStarting'] as bool? ?? false,
    themeMode: _$enumDecodeNullable(_$ThemeModeEnumMap, json['themeMode']) ??
        ThemeMode.system,
    errorMessage: json['errorMessage'] as String?,
  );
}

Map<String, dynamic> _$_$_RootStateToJson(_$_RootState instance) =>
    <String, dynamic>{
      'isStarting': instance.isStarting,
      'themeMode': _$ThemeModeEnumMap[instance.themeMode],
      'errorMessage': instance.errorMessage,
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

K? _$enumDecodeNullable<K, V>(
  Map<K, V> enumValues,
  dynamic source, {
  K? unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<K, V>(enumValues, source, unknownValue: unknownValue);
}

const _$ThemeModeEnumMap = {
  ThemeMode.system: 'system',
  ThemeMode.light: 'light',
  ThemeMode.dark: 'dark',
};
