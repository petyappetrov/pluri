import 'package:pluri/root/root.dart';

RootState rootReducer(RootState state, action) {
  if (action is RootStartupApplicationAction) {
    return state.copyWith(
      isStarting: true,
    );
  }
  if (action is RootStartupApplicationSuccessAction) {
    return state.copyWith(
      isStarting: false,
    );
  }
  if (action is RootStartupApplicationFailureAction) {
    return state.copyWith(
      isStarting: false,
      errorMessage: action.errorMessage,
    );
  }
  if (action is RootChangeThemeModeAction) {
    return state.copyWith(
      themeMode: action.themeMode,
    );
  }
  return state;
}
